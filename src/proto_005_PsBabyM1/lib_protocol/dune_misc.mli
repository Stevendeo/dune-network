(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type error +=
  | Failure of string (* `Permanent *)

val failwith :
  ('a, Format.formatter, unit, 'b tzresult Lwt.t) format4 -> 'a

module type BINARY_JSON = sig
  type t
  val encoding : t Data_encoding.t
  val encode : t -> MBytes.t
  val decode : MBytes.t -> t tzresult Lwt.t
  val decode_exn : MBytes.t -> t
  val destruct_exn : Data_encoding.json -> t
  val pp : Format.formatter -> t -> unit
end

module MakeBinaryJson(S : sig
    type t
    val encoding : t Data_encoding.t
    val name : string
  end) : BINARY_JSON with type t := S.t
