(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let bool = 1
let int8 = 1
let uint8 = 1
let char = 1
let int16 = 2
let uint16 = 2
let uint30 = 4
let uint32 = 4
let uint64 = 8
let int31 = 4
let int32 = 4
let int64 = 8
let float = 8

type signed_integer = [ `Int31 | `Int16 | `Int8 ]
type unsigned_integer = [ `Uint30 | `Uint16 | `Uint8 ]
type integer = [ signed_integer | unsigned_integer ]

let integer_to_size = function
  | `Int31 -> int31
  | `Int16 -> int16
  | `Int8 -> int8
  | `Uint30 -> uint30
  | `Uint16 -> uint16
  | `Uint8 -> uint8

let max_int = function
  | `Uint30 | `Int31 -> (1 lsl 30) - 1
  | `Int16 -> 1 lsl 15 - 1
  | `Int8 -> 1 lsl 7 - 1
  | `Uint16 -> 1 lsl 16 - 1
  | `Uint8 -> 1 lsl 8 - 1

let min_int = function
  | `Uint8 | `Uint16 | `Uint30 -> 0
  | `Int31 -> - (1 lsl 30)
  | `Int16 -> - (1 lsl 15)
  | `Int8 -> - (1 lsl 7)
