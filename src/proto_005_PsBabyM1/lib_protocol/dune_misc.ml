(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type error +=
  | Failure of string (* `Permanent *)

let () =
  register_error_kind
    `Permanent
    ~id:"misc.failure"
    ~title:"failure"
    ~description:"Unexpected error"
    ~pp:(fun ppf s -> Format.fprintf ppf "Misc.Failure %S" s)
    Data_encoding.(obj1 (req "message" string))
    (function Failure s -> Some s | _ -> None)
    (fun s -> Failure s) ;
  ()

let failwith fmt =
  Format.kasprintf (fun msg -> fail (Failure msg)) fmt

module type BINARY_JSON = sig
  type t
  val encoding : t Data_encoding.t
  val encode : t -> MBytes.t
  val decode : MBytes.t -> t tzresult Lwt.t
  val decode_exn : MBytes.t -> t
  val destruct_exn : Data_encoding.json -> t
  val pp : Format.formatter -> t -> unit
end

module MakeBinaryJson(S : sig
    type t
    val encoding : t Data_encoding.t
    val name : string
  end) : BINARY_JSON with type t := S.t
= struct
  let encoding = S.encoding
  let encode options =
    let json =
      Data_encoding.Json.construct S.encoding options
    in
    Data_encoding.Binary.to_bytes_exn Data_encoding.json json

  let decode_exn bytes =
    match Data_encoding.Binary.of_bytes Data_encoding.json bytes with
    | None ->
        Format.kasprintf Pervasives.failwith
          "Failed to decode %s binary json"
          S.name
    | Some json ->
        match Data_encoding.Json.destruct S.encoding json with
        | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
            Format.kasprintf Pervasives.failwith
              "Failed to parse dune_manage_account json: %a %a"
              (fun ppf -> Data_encoding.Json.print_error ppf) exn
              Data_encoding.Json.pp json
        | options -> options

  let decode bytes =
    match decode_exn bytes with
    | exception exn -> failwith "%s" (Dune_debug.string_of_exn exn)
    | v -> return v

  let destruct_exn json =match Data_encoding.Json.destruct S.encoding json
    with
    | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
        Format.kasprintf Pervasives.failwith
          "Invalid %s: %a %a" S.name
          (fun ppf -> Data_encoding.Json.print_error ppf) exn
          Data_encoding.Json.pp json
    |  options -> options

  let pp ppf options =
    let json =
      Data_encoding.Json.construct S.encoding options
    in
    Data_encoding.Json.pp ppf json
end
