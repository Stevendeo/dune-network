(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)


module Options : sig

  (** Activates debug messages *)
  val debug : bool ref

  val max_stack : int ref

  val version : int * int

end


module Exceptions : sig
  exception StackOverflow
  exception InvariantBroken of string
  exception GenericError of string
  exception BadArgument of string
  exception Uncomparable
  exception BadParameter
  exception BadReturnType
  exception BadStorage
  exception BadEntryPoint
  exception BadView
  exception DeserializeError of int * string
  exception DecodingError of string
  exception ParseError of string
end


module Log : sig

  val dummy_formatter : Format.formatter

  val fmt : (unit -> Format.formatter) ref

  val print : ('a, Format.formatter, unit) format -> 'a

  val debug : ('a, Format.formatter, unit) format -> 'a

end


module Utils : sig

  type ('a, 'annot) annoted = {
    content: 'a;
    annot : 'annot
  }

  val is_empty : 'a list -> bool

  val flags_to_bits : int -> bool list -> int

  val bits_to_flags : int -> int -> bool list

  val add_uniq : 'a -> 'a list -> 'a list

  val add_uniq_lst : 'a list -> 'a list -> 'a list

  val print_list_s :
    (unit, Format.formatter, unit) format ->
    (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit

  val bindings_to_map : ('a -> 'b -> 'c -> 'c) -> 'c -> ('a * 'b) list -> 'c

  val list_equal : 'a list -> 'a list -> bool

  val list_strict_equal : ('a -> 'a -> bool) -> 'a list -> 'a list -> bool

  val wrap_debug :
    bool ->
    Format.formatter ->
    string -> string -> (Format.formatter -> 'a -> unit) -> 'a -> unit

  val wrap_debug_left :
    bool ->
    Format.formatter ->
    string -> (Format.formatter -> 'a -> unit) -> 'a -> unit

  val wrap_debug_right :
    bool ->
    Format.formatter ->
    string -> (Format.formatter -> 'a -> unit) -> 'a -> unit

  val address_of_string_opt :
    string -> Contract_repr.t option

  val compare_list : ('a -> 'a -> int) -> 'a list -> 'a list -> int

  val compare_pair : ('a -> 'a -> int) -> ('b -> 'b -> int) -> ('a * 'b) -> ('a * 'b) -> int

  val compare_option : ('a -> 'a -> int) -> 'a option -> 'a option -> int

end


module Ident : sig

  (** The type of identifier *)
  type 'a t

  (** Comparison of identifiers *)
  val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool

  (** Utilities *)
  val create_id : 'a -> 'a t
  val put_in_namespace : 'a -> 'a t -> 'a t
  val put_in_namespaces : 'a list -> 'a t -> 'a t
  val create_namespace : 'a -> 'a t list -> 'a t list
  val iter : ('a -> unit) -> 'a t -> unit
  val fold : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val map : ('a -> 'b) -> 'a t -> 'b t
  val get_final : 'a t -> 'a
  val split : 'a t -> 'a * 'a t option
  val get_list : 'a t -> 'a list
  val put_list : 'a list -> 'a t
  val concat : 'a t -> 'a t -> 'a t
  val change_last : 'a t -> 'a t -> 'a t

  val pretty : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit


  module IdSet : functor (C : Compare.COMPARABLE) -> S.SET with type elt = C.t t
  module IdMap : functor (C : Compare.COMPARABLE) -> S.MAP with type key = C.t t

  val print_strident : Format.formatter -> string t -> unit

end


module Path : sig
  type path_item =
    | Prev
    | Next of string

  type t = path_item list
  val pp_path : Format.formatter -> t -> unit
  val eq_path : t -> t -> bool
  val simplify_path : t -> t
  val path_of_id : string Ident.t -> t * string
end


module Collections : sig

  module IntSet : S.SET with type elt = int
  module IntMap : S.MAP with type key = int

  module StringSet : S.SET with type elt = String.t
  module StringMap : sig
    include S.MAP with type key = String.t
    val add_list : (key * 'a) list -> 'a t -> 'a t
  end

  module StringIdentSet : S.SET with type elt = String.t Ident.t
  module StringIdentMap : sig
    include S.MAP with type key = String.t Ident.t
    val add_list : (key * 'a) list -> 'a t -> 'a t
  end

  module ZSet : S.SET with type elt = Z.t

  module Array : sig
    type 'a t
    val length : 'a t -> int
    val make : int -> 'a -> 'a t
    val get : 'a t -> int -> 'a
    val set : 'a t -> int -> 'a -> unit
  end

end



module Constants : sig
  val current : string
  val anonymous : string
  val current_id : string Ident.t
  val anonymous_id : string Ident.t
end
