(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Exceptions
open Utils
open Love_runtime_ast
open Signature

module rec Value : sig

  type ('a, 'b) local_or_global =
    | Local of 'a
    | Global of 'b

  type 'a ptr_or_inlined =
    | Pointer of string Ident.t
    | Inlined of string Ident.t * 'a

  type val_contents =
    (Value.t, Value.t Value.ptr_or_inlined) Value.local_or_global

  type env = {
    values: (string Ident.t * val_contents) list;
    structs: (string Ident.t * LiveStructure.t ptr_or_inlined) list;
    sigs: (string Ident.t * Love_type.structure_sig ptr_or_inlined) list;
    exns: (string Ident.t * string Ident.t) list;
    types: (string Ident.t * string Ident.t) list;
    tvars: (string * Love_type.t) list;
  }

  type closure = {
    call_env: env;
    lambda: lambda }

  type bigmap = {
    id: Z.t option;
    diff: Value.t option ValueMap.t;
    key_type: Love_type.t;
    value_type: Love_type.t }

  type t =
    (* Base *)
    | VUnit
    | VBool of bool
    | VString of string
    | VBytes of MBytes.t
    | VInt of Z.t
    | VNat of Z.t

    (* Composite *)
    | VNone
    | VSome of t
    | VTuple of t list
    | VConstr of string * t list
    | VRecord of (string * t) list

    (* Collections *)
    | VList of t list
    | VSet of ValueSet.t
    | VMap of t ValueMap.t
    | VBigMap of bigmap

    (* Domain-specific *)
    | VDun of Tez_repr.t
    | VKey of Public_key.t
    | VKeyHash of Public_key_hash.t
    | VSignature of Signature.t
    | VTimestamp of Script_timestamp_repr.t
    | VAddress of Contract_repr.t
    | VOperation of Op.t

    (* Structures and entry points *)
    | VPackedStructure of (string Ident.t * LiveContract.t)
    | VContractInstance of Love_type.structure_sig * Contract_repr.t
    | VEntryPoint of Contract_repr.t * string
    | VView of Contract_repr.t * string

    (* Closures *)
    | VClosure of closure

    (* Love primitive
       VPrimitive (f, [a1;a2;...]) = f a1 a2 ...*)
    | VPrimitive of (Love_primitive.t * t list)

  val compare: t -> t -> int
  val equal: t -> t -> bool
  val of_const: const -> t
  val unrec_closure: t -> t
  val rec_closure: t -> t

end = struct

  type ('a, 'b) local_or_global =
    | Local of 'a
    | Global of 'b

  type 'a ptr_or_inlined =
    | Pointer of string Ident.t
    | Inlined of string Ident.t * 'a

  type val_contents =
    (Value.t, Value.t Value.ptr_or_inlined) Value.local_or_global

  type env = {
    values: (string Ident.t * val_contents) list;
    structs: (string Ident.t * LiveStructure.t ptr_or_inlined) list;
    sigs: (string Ident.t * Love_type.structure_sig ptr_or_inlined) list;
    exns: (string Ident.t * string Ident.t) list;
    types: (string Ident.t * string Ident.t) list;
    tvars: (string * Love_type.t) list;
  }

  type closure = {
    call_env: env;
    lambda: lambda }

  type bigmap = {
    id: Z.t option;
    diff: Value.t option ValueMap.t;
    key_type: Love_type.t;
    value_type: Love_type.t }

  type t =
    | VUnit
    | VBool of bool
    | VString of String.t
    | VBytes of MBytes.t
    | VInt of Z.t
    | VNat of Z.t

    (* Composite *)
    | VNone
    | VSome of t
    | VTuple of t list
    | VConstr of string * t list
    | VRecord of (string * t) list

    (* Collections *)
    | VList of t list
    | VSet of ValueSet.t
    | VMap of t ValueMap.t
    | VBigMap of bigmap

    (* Domain-specific *)
    | VDun of Tez_repr.t
    | VKey of Public_key.t
    | VKeyHash of Public_key_hash.t
    | VSignature of Signature.t
    | VTimestamp of Script_timestamp_repr.t
    | VAddress of Contract_repr.t
    | VOperation of Op.t

    (* Structures and entry points *)
    | VPackedStructure of (string Ident.t * LiveContract.t)
    | VContractInstance of Love_type.structure_sig * Contract_repr.t
    | VEntryPoint of Contract_repr.t * string
    | VView of Contract_repr.t * string

    (* Closures *)
    | VClosure of closure

    (* Love primitive
       VPrimitive (f, [a1;a2;...]) = f a1 a2 ...*)
    | VPrimitive of (Love_primitive.t * t list)

  let rec compare c1 c2 =
    let compare_const_list = compare_list compare in
    let compare_field_list = compare_list compare_field in
    match c1, c2 with
    | VUnit, VUnit -> 0
    | VBool b1, VBool b2 -> Compare.Bool.compare b1 b2
    | VString s1, VString s2 -> String.compare s1 s2
    | VBytes s1, VBytes s2 -> MBytes.compare s1 s2
    | VInt i1, VInt i2 -> Z.compare i1 i2
    | VNat i1, VNat i2 -> Z.compare i1 i2
    | VNone, VNone -> 0
    | VSome c1, VSome c2 -> compare c1 c2
    | VTuple cl1, VTuple cl2 -> compare_const_list cl1 cl2
    | VConstr (c1, cl1), VConstr (c2, cl2) ->
        let res = String.compare c1 c2 in
        if Compare.Int.(res <> 0) then res
        else compare_const_list cl1 cl2
    | VRecord fcl1, VRecord fcl2 ->
        let cl1 =
          List.fast_sort (fun (f1, _) (f2, _) -> String.compare f1 f2) fcl1 in
        let cl2 =
          List.fast_sort (fun (f1, _) (f2, _) -> String.compare f1 f2) fcl2 in
      compare_field_list cl1 cl2
    | VList cl1, VList cl2 -> compare_const_list cl1 cl2
    | VSet cs1, VSet cs2 -> ValueSet.compare cs1 cs2
    | VMap cm1, VMap cm2 -> ValueMap.compare compare cm1 cm2
    | VDun d1, VDun d2 -> Tez_repr.compare d1 d2
    | VKey k1, VKey k2 -> Public_key.compare k1 k2
    | VKeyHash kh1, VKeyHash kh2 -> Public_key_hash.compare kh1 kh2
    | VSignature sig1, VSignature sig2 -> Signature.compare sig1 sig2
    | VTimestamp ts1, VTimestamp ts2 -> Script_timestamp_repr.compare ts1 ts2
    | VAddress a1, VAddress a2 -> Contract_repr.compare a1 a2
    | VContractInstance (_, a1), VContractInstance (_, a2) ->
        Contract_repr.compare a1 a2
    | VEntryPoint (a1, n1), VEntryPoint (a2, n2)
    | VView (a1, n1), VView (a2, n2) ->
        let res = Contract_repr.compare a1 a2 in
        if Compare.Int.(res <> 0) then res
        else String.compare n1 n2
    | VBigMap _, VBigMap _ -> raise Uncomparable
    | VPackedStructure _, VPackedStructure _ -> raise Uncomparable
    | VOperation _, VOperation _ -> raise Uncomparable
    | VClosure _, VClosure _ -> raise Uncomparable
    | VPrimitive _, VPrimitive _ -> raise Uncomparable

    (* Exhaustiveness *)
    | (VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _ |
       VNone | VSome _ | VTuple _ | VConstr _ | VRecord _ | VList _ |
       VSet _ | VMap _ | VBigMap _ | VDun _ | VKey _ | VKeyHash _ |
       VSignature _ | VTimestamp _ | VAddress _ | VOperation _ |
       VContractInstance _ | VPackedStructure _ | VEntryPoint _ | VView _ |
       VClosure _ | VPrimitive _),
      (VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _ |
       VNone | VSome _ | VTuple _ | VConstr _ | VRecord _ | VList _ |
       VSet _ | VMap _ | VBigMap _ | VDun _ | VKey _ | VKeyHash _ |
       VSignature _ | VTimestamp _ | VAddress _ | VOperation _ |
       VContractInstance _ | VPackedStructure _ | VEntryPoint _ | VView _ |
       VClosure _ | VPrimitive _) ->
      raise Uncomparable

  and compare_field (f1, v1) (f2, v2) =
    if String.equal f1 f2 then compare v1 v2
    else raise Uncomparable

  let equal v1 v2 =
    Compare.Int.(Value.compare v1 v2 = 0)

  let of_const = function
    | RCUnit -> VUnit
    | RCBool b -> VBool b
    | RCString s -> VString s
    | RCBytes b -> VBytes b
    | RCInt i -> VInt i
    | RCNat i -> VNat i
    | RCDun d -> VDun d
    | RCKey k -> VKey k
    | RCKeyHash kh -> VKeyHash kh
    | RCSignature s -> VSignature s
    | RCTimestamp t -> VTimestamp t
    | RCAddress a -> VAddress a
    | RCPrimitive p ->
        if Compare.Int.(Love_primitive.arity p = 0) then
          match p with
          | PSEmpty -> VSet (ValueSet.empty)
          | PMEmpty -> VMap (ValueMap.empty)
          | PBMEmpty (tk, tv) -> VBigMap { id = None; diff = ValueMap.empty;
                                           key_type = tk; value_type = tv }
          | _ -> raise (InvariantBroken ("Unhandled constant primitive : " ^
                                         (Love_primitive.name p)))
        else VPrimitive (p, [])

  let unrec_closure v =
    match v with
    | VClosure { call_env; lambda } ->
        let is_rec = ref false in
        let values = List.map (fun id_vc ->
            match id_vc with
            | id, Local v' ->
                if v == v' then
                  begin
                    is_rec := true;
                    (Ident.create_id "@self",
                     Local (VList (List.map (fun s ->
                         VString s) (Ident.get_list id))))
                  end
                else id_vc
            | _ -> id_vc
          ) call_env.values
        in
        if !is_rec then
          VClosure { call_env = { call_env with values }; lambda }
        else v
    | _ -> v

  let rec_closure v =
    match v with
    | VClosure { call_env; lambda } ->
        let values, self = List.partition (fun (id, _c) ->
          not (String.equal (Ident.get_final id) ("@self"))) call_env.values in
        begin match self with
          | [] -> v
          | [_, Local (VList id)] ->
              let id = Ident.put_list (List.map (function
                  | VString s -> s
                  | _ -> raise (InvariantBroken "Bad recusrive closure")
                ) id)
              in
              let rec c =
                VClosure { call_env =
                             { call_env with
                               values = (id, Local c) :: values };
                           lambda } in
              c
          | _ -> raise (InvariantBroken "Bad recursive function")
        end
    | _ -> v

end

and ValueSet : S.SET with type elt = Value.t = Set.Make (Value)

and ValueMap : S.MAP with type key = Value.t = Map.Make (Value)

and Op : sig
  type manager_operation =
    | Origination of {
        delegate: Public_key_hash.t option;
        script: (Value.t * LiveContract.t); (* Initial storage + contract *)
        credit: Tez_repr.t;
        preorigination: Contract_repr.t option;
      }
    | Transaction of {
        amount: Tez_repr.t; (* The amount of the transaction *)
        parameters: Value.t option; (* An optional parameter *)
        entrypoint: string;
        destination: Contract_repr.t; (* The transaction receiver. *)
        (* collect call *)
      }
    | Delegation of Public_key_hash.t option
    | Dune_manage_account of
        { target : Public_key_hash.t option;
          maxrolls : int option option;
          admin : Contract_repr.t option option;
          white_list : Contract_repr.t list option;
          delegation : bool option }

  type internal_operation = {
      source: Contract_repr.t;
      operation: manager_operation;
      nonce: int;
    }

  type big_map_diff_item =
    | Update of {
        big_map : Z.t;
        diff_key : Value.t;
        diff_key_hash : Script_expr_hash.t;
        diff_value : Value.t option;
      }
    | Clear of Z.t
    | Copy of Z.t * Z.t
    | Alloc of {
        big_map : Z.t;
        key_type : Love_type.t;
        value_type : Love_type.t;
      }

  type big_map_diff = big_map_diff_item list

  type t = internal_operation * big_map_diff option

end = Op

and LiveStructure : sig
  type entry = {
    ventry_code: Value.t;
    ventry_fee_code: Value.t option;
    ventry_typ: Love_type.t;
  }

  type view = {
    vview_code: Value.t;
    vview_typ: Love_type.t * Love_type.t;
  }

  type value = {
    vvalue_code: Value.t;
    vvalue_typ: Love_type.t;
    vvalue_visibility: visibility;
  }

  type content =
    | VType of Love_runtime_ast.type_kind * Love_type.typedef
    | VException of Love_type.t list
    | VEntry of entry
    | VView of view
    | VValue of value
    | VStructure of LiveStructure.t
    | VSignature of Love_type.structure_sig

  and t = {
    kind : Love_type.struct_kind;
    content : (string * content) list;
  }

  val unit: t

  val resolve_id_in_struct: LiveStructure.t -> string Ident.t -> content option

  val is_module : t -> bool

end = struct
  type entry = {
    ventry_code: Value.t;
    ventry_fee_code: Value.t option;
    ventry_typ: Love_type.t;
  }

  type view = {
    vview_code: Value.t;
    vview_typ: Love_type.t * Love_type.t;
  }

  type value = {
    vvalue_code: Value.t;
    vvalue_typ: Love_type.t;
    vvalue_visibility: visibility;
  }

  type content =
    | VType of Love_runtime_ast.type_kind * Love_type.typedef
    | VException of Love_type.t list
    | VEntry of entry
    | VView of view
    | VValue of value
    | VStructure of LiveStructure.t
    | VSignature of Love_type.structure_sig

  and t = {
    kind : Love_type.struct_kind;
    content : (string * content) list;
  }

  let unit = { content = []; kind = Contract [] }

  let resolve_id_in_struct (s : LiveStructure.t) (id : string Ident.t) =
    let open LiveStructure in
    List.fold_left (fun res n ->
        match res with
        | Some (VStructure s) -> List.assoc_opt n s.content
        | Some _ | None -> None
      ) (Some (VStructure s)) (Ident.get_list id)

  let is_module c = match c.kind with Module -> true | Contract _ -> false

end

and LiveContract : sig

  type t = {
    version : (int * int); (* (major, minor) *)
    root_struct : LiveStructure.t;
  }

  val unit : t

end = struct

  type t = {
    version : (int * int); (* (major, minor) *)
    root_struct : LiveStructure.t;
  }

  let unit = { version = Options.version; root_struct = LiveStructure.unit }

end

and FeeCode : sig

  type t = {
    version : (int * int); (* (major, minor) *)
    root_struct : LiveStructure.t; (* Containing only actually used code/data *)
    fee_codes : (string * (Value.t * Love_type.t)) list
  }

end = FeeCode


let normalize_contract (contract : LiveContract.t) =
  contract, FeeCode.{ version = contract.version;
    root_struct = LiveStructure.unit;
    fee_codes = [];
  }

let rec sig_of_structure ~only_typedefs
    LiveStructure.{ content; kind } : Love_type.structure_sig =
  let open LiveStructure in
  let open Love_type in
  let has_storage = ref false in
  let sig_content =
    List.fold_left (fun acc (n, content) ->
        match content with
        | VType (TInternal, _td) -> acc
        | VType (TPublic, td) ->
          let () =
            match kind with
              Contract _ when String.equal n "storage" -> has_storage := true
            | _ -> () in
          (n, SType (SPublic td)) :: acc
        | VType (TPrivate, td) -> (n, SType (SPrivate td)) :: acc
        | VType (TAbstract, td) ->
           let params = match td with
             | Alias { aparams = p; _ }
             | SumType { sparams = p; _ }
             | RecordType { rparams = p; _ } -> p in
           (n, SType (SAbstract params)) :: acc
        | VException tl when not only_typedefs ->
          (n, SException tl) :: acc
        | VEntry { ventry_typ = tparam;_ } when not only_typedefs ->
          (n, SEntry tparam) :: acc
        | VView { vview_typ = (tparam, treturn);_ }  when not only_typedefs ->
          (n, SView (tparam, treturn)) :: acc
        | VValue { vvalue_typ; vvalue_visibility = Public; _ }
          when not only_typedefs ->
          (n, SValue vvalue_typ) :: acc
        | VValue { vvalue_visibility = Private; _ } -> acc
        | VStructure s ->
          (n, SStructure (Anonymous (sig_of_structure ~only_typedefs s))) :: acc
        | VSignature s ->
          (n, SSignature s) :: acc
        | _ -> acc
      )
      []
      content
  in
  Love_type.{
    sig_content = List.rev sig_content;
    sig_kind = kind
  }

type val_or_type =
  | Value of Value.t
  | Type of Love_type.t
