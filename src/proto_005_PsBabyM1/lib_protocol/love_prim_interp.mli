(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

val eval :
  (Love_context.t ->
   Love_env.t ->
   Love_value.Value.t ->
   Love_context.v ->
   (Love_context.t * Love_value.Value.t) tzresult Lwt.t) ->
  Love_context.t ->
  Love_env.t ->
  Love_primitive.t ->
  Love_value.Value.t list ->
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t

val collect_big_maps :
  Love_context.t ->
  Love_value.Value.t ->
  (Love_pervasives.Collections.ZSet.t * Love_context.t) tzresult
    Lwt.t

val extract_big_map_diff :
  Love_context.t ->
  temporary:bool ->
  to_duplicate:Love_pervasives.Collections.ZSet.t ->
  to_update:Love_pervasives.Collections.ZSet.t ->
  Love_value.Value.t ->
  (Love_value.Value.t *
   Love_value.Op.big_map_diff_item list option * Love_context.t)
    tzresult Lwt.t

val register_primitive :
  Love_primitive.generic_primitive ->
  Alpha_context.Gas.cost ->
  ((Love_context.t ->
    Love_env.t ->
    Love_value.ValueSet.elt ->
    Love_context.v ->
    (Love_context.t * Love_value.ValueSet.elt) tzresult Lwt.t) ->
   Love_context.t ->
   Love_env.t ->
   Love_primitive.generic_primitive ->
   Love_value.ValueSet.elt list ->
   (Love_context.t * Love_value.ValueSet.elt)) ->
  unit
