(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type primitive_kind =
  | PrimFunction
  | PrimInfix
  | PrimPrefix

type generic_primitive = {
  prim_name : string ;
  prim_kind : primitive_kind ;
  prim_type : Love_type.t ;
  prim_arity : int ;
  prim_id : int ;
}

type t =
  (* Comparisons *)
  | PCompare | PEq | PNe | PLt | PLe | PGt | PGe

  (* Arithmetic on Integers/Naturals/Duns/Timestamps *)
  | PIAdd  | PISub  | PIMul  | PIDiv  | PINeg | PIAbs
  | PNAdd  | PNSub  | PNMul  | PNDiv  | PNNeg | PNAbs
  | PNIAdd | PNISub | PNIMul | PNIDiv
  | PINAdd | PINSub | PINMul | PINDiv
  | PDAdd  | PDSub  | PDDiv
  | PDIMul | PIDMul | PDIDiv
  | PDNMul | PNDMul | PDNDiv
  | PTSub
  | PTIAdd | PITAdd | PTISub
  | PTNAdd | PNTAdd | PTNSub

  (* Boolean operators *)
  | PBAnd | PBOr | PBXor | PBNot

  (* Bitwise operators *)
  | PIAnd | PIOr | PIXor | PINot | PILsl | PILsr
  | PNAnd | PNOr | PNXor | PNLsl | PNLsr

  (* List primitives *)
  | PLLength | PLCons | PLRev | PLConcat
  | PLIter | PLMap | PLFold | PLMapFold

  (* Set primitives *)
  | PSEmpty | PSCard | PSAdd | PSRemove | PSMem
  | PSIter | PSMap | PSFold | PSMapFold

  (* Map primitives *)
  | PMEmpty | PMCard | PMAdd | PMRemove | PMMem | PMFind
  | PMIter | PMMap | PMFold | PMMapFold

  (* Map primitives *)
  | PBMEmpty of Love_type.t * Love_type.t
  | PBMAdd | PBMRemove | PBMMem | PBMFind

  (* Loop primitive *)
  | PLLoop

  (* String primitives *)
  | PSLength | PSConcat | PSSlice

  (* Bytes primitives *)
  | PBLength | PBConcat | PBSlice
  | PBPack | PBUnpack of Love_type.t
  | PBHash

  (* Cryptographic operations *)
  | PCBlake2b | PCSha256 | PCSha512
  | PCHashKey | PCCheck

  (* Contract interactions *)
  | PCAddress
  | PCSelf of Love_type.contract_type
  | PCAt of Love_type.contract_type
  | PCCall | PCView | PCCreate | PCSetDeleg

  (* Account interactions *)
  | PADefault | PATransfer | PABalanceOf
  | PAGetInfo | PAManage

  (* General information *)
  | PCBalance (* balance of current contract *)
  | PCTime (* timestamp of block in which the transaction is included *)
  | PCAmount (* amount of dun transferred by the current operation *)
  | PCGas (* amount of gas remaining to execute the rest of the transaction *)
  | PCrSelf (* address of current contract *)
  | PCSource (* address that initiated the current top-level transaction *)
  | PCSender (* address that initiated the current transaction *)
  | PCLevel (* current level *)
  | PCCycle (* current cycle *)

  (* Conversions *)
  | PAddressOfKeyHash
  | PKeyHashOfAddress

  | PGenericPrimitive of generic_primitive

val max_possible_prim_id : int

val from_string : string -> generic_primitive option

(* Do not use this function, use Love_prim_interp.register_primitive instead *)
val add_primitive : generic_primitive -> unit

val name : t -> string
val compare : t -> t -> int
val arity : t -> int
val type_of :
  ?sig_of_contract:(Love_type.structure_type_name ->
                    Love_type.structure_sig) ->
  t -> Love_type.t
val id_of_prim : t -> int
val is_infix : t -> bool
val is_prefix : t -> bool
val prim_of_id : int -> t

val infix_of_string : string -> t
val prefix_of_string : string -> t
val of_string : string -> t
