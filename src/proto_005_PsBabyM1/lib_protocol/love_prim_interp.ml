(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Error_monad
open Alpha_context
open Love_pervasives
open Exceptions
open Love_context
open Love_primitive
open Love_value
open Value
open Collections

let generic_primitives =
  Array.make (Love_primitive.max_possible_prim_id + 1)
    (Gas.step_cost 1,  fun _ -> assert false)

let register_primitive p gas f =
  Love_primitive.add_primitive p;
  generic_primitives.(p.prim_id) <- ( gas , f )

let consume_gas ctxt gas =
  Lwt.return (Alpha_context.Gas.consume ctxt.actxt gas
              >|? fun actxt -> { ctxt with actxt })

let sig_of_sigref ctxt env =
  let open Love_type in
  function
  | Anonymous s -> return (ctxt, s)
  | Named id ->
      Love_env.find_sig_opt ctxt env id >>|? fun (ctxt, res) ->
      match res with
      | Some s -> ctxt, s
      | None -> raise (InvariantBroken "Signature not found")


let hash_data ctxt k =
  match Data_encoding.Binary.to_bytes Love_encoding.Value.encoding k with
  | None -> raise (InvariantBroken "Can't encode value")
  | Some res ->
      consume_gas ctxt (Love_gas.Cost_of.hash res Script_expr_hash.size)
      >>=? fun ctxt -> return (ctxt, Script_expr_hash.hash_bytes [res])

let empty_big_map tk tv =
  { id = None; diff = Love_value.ValueMap.empty;
    key_type = tk; value_type = tv }

let big_map_mem (ctxt : Love_context.t) { id; diff; _ } key =
  match Love_value.ValueMap.find_opt key diff, id with
  | None, None -> return (ctxt, false)
  | None, Some id ->
      hash_data ctxt key >>=? fun (ctxt, hash) ->
      Alpha_context.Big_map.mem ctxt.actxt id hash
      >>=? fun (actxt, res) -> return ({ctxt with actxt = actxt}, res)
  | Some None, _ -> return (ctxt, false)
  | Some (Some _), _ -> return (ctxt, true)

let big_map_get (ctxt : Love_context.t) { id; diff; _ } key =
  match Love_value.ValueMap.find_opt key diff, id with
  | Some (Some res), _ -> return (ctxt, VSome res)
  | Some None, _ -> return (ctxt, VNone)
  | None, None -> return (ctxt, VNone)
  | None, Some id ->
      begin hash_data ctxt key >>=? fun (ctxt, hash) ->
        Alpha_context.Big_map.get_opt ctxt.actxt id hash
        >>=? fun (actxt, res) -> match res with
        | None -> return ({ ctxt with actxt = actxt }, VNone)
        | Some (Script_all.Dune_code _) -> assert false
        | Some (Dune_expr e) ->
            begin match Love_repr.is_const e with
              | None -> return (ctxt, VNone)
              | Some (Value value) -> return (ctxt, VSome value)
              | Some (Type _) -> assert false
            end
        | Some (Michelson_expr _) -> assert false
      end

let big_map_add ctxt bm key bnd =
(* No inlining of key as it does not contain lambdas *)
(* No inlining of bnd : this will be done when passing
   to another contract or when storing the bigmap *)
  (* let bnd = Love_translator.inline_value ctxt bnd in *)
  let new_bm = { bm with diff = ValueMap.add key (Some bnd) bm.diff } in
  return (ctxt, new_bm)

let big_map_del ctxt bm key =
  let new_bm = { bm with diff = ValueMap.add key None bm.diff} in
  return (ctxt, new_bm)


let diff_of_big_map ctxt fresh ~ids Value.{ id; key_type; value_type; diff } =
  let open Collections in
  consume_gas ctxt (Love_gas.Cost_of.construct (ValueMap.cardinal diff))
  >>=? fun ctxt ->
  begin match id with
    | Some id ->
        if ZSet.mem id ids then
          fresh ctxt >>=? fun (ctxt, duplicate) ->
          return (ctxt, [ Op.Copy (id, duplicate) ], duplicate)
        else
          (* The first occurence encountered of a big_map reuses the
             ID. This way, the payer is only charged for the diff.
             For this to work, this diff has to be put at the end of
             the global diff, otherwise the duplicates will use the
             updated version as a base. This is true because we add
             this diff first in the accumulator of
             `extract_big_map_updates`, and this accumulator is not
             reversed before being flattened. *)
          return (ctxt, [], id)
    | None ->
        fresh ctxt >>=? fun (ctxt, id) ->
        return (ctxt, [ Op.Alloc { big_map = id; key_type; value_type } ], id)
  end >>=? fun (ctxt, init, big_map) ->
  let pairs =
    ValueMap.fold (fun key value acc -> (key, value) :: acc) diff [] in
  fold_left_s (fun (acc, ctxt) (diff_key, (diff_value : Value.t option)) ->
      consume_gas ctxt Love_gas.Cost_of.cycle >>=? fun ctxt ->
      hash_data ctxt diff_key >>=? fun (ctxt, diff_key_hash) ->
      let diff_item =
        Op.Update { big_map; diff_key; diff_key_hash; diff_value } in
      return (diff_item :: acc, ctxt))
    ([], ctxt) pairs >>=? fun (diff, ctxt) ->
  return (init @ diff, big_map, ctxt)

let extract_big_map_updates ctxt fresh ids acc v =
  let open Value in
  let open Collections in
  let rec aux_list ctxt ids acc vl =
    fold_left_s (fun (ctxt, vl, ids, acc) v ->
        consume_gas ctxt Love_gas.Cost_of.cycle >>=? fun ctxt ->
        aux ctxt fresh ids acc v >>|? fun (ctxt, v, ids, acc) ->
        (ctxt, v :: vl, ids, acc)
      ) (ctxt, [], ids, acc) vl
  and aux ctxt fresh ids acc v =
    match Value.unrec_closure v with
    | VBigMap vbm ->
        diff_of_big_map ctxt fresh ids vbm >>=? fun (diff, id, ctxt) ->
        let vbm = { vbm with diff = ValueMap.empty; id = Some id } in
        return (ctxt, VBigMap vbm, ZSet.add id ids, diff :: acc)

    | VSome v ->
        consume_gas ctxt Love_gas.Cost_of.cycle >>=? fun ctxt ->
        aux ctxt fresh ids acc v >>|? fun (ctxt, v, ids, acc) ->
        (ctxt, VSome v, ids, acc)

    | VTuple vl ->
        aux_list ctxt ids acc vl >>|? fun (ctxt, vl, ids, acc) ->
        (ctxt, VTuple (List.rev vl), ids, acc)

    | VList vl ->
        aux_list ctxt ids acc vl >>|? fun (ctxt, vl, ids, acc) ->
        (ctxt, VList (List.rev vl), ids, acc)

    | VConstr (cstr, vl) ->
        aux_list ctxt ids acc vl >>|? fun (ctxt, vl, ids, acc) ->
        (ctxt, VConstr (cstr, List.rev vl), ids, acc)

    | VPrimitive (prim, vl) ->
        aux_list ctxt ids acc vl >>|? fun (ctxt, vl, ids, acc) ->
        (ctxt, VPrimitive (prim, List.rev vl), ids, acc)

    | VRecord fvl ->
        fold_left_s (fun (ctxt, fvl, ids, acc) (f, v) ->
          consume_gas ctxt Love_gas.Cost_of.cycle >>=? fun ctxt ->
          aux ctxt fresh ids acc v >>|? fun (ctxt, v, ids, acc) ->
          (ctxt, (f, v) :: fvl, ids, acc)
        ) (ctxt, [], ids, acc) fvl >>|? fun (ctxt, fvl, ids, acc) ->
        (ctxt, VRecord (List.rev fvl), ids, acc)

    | VMap vm -> (* Key should not contain bigmaps *)
        fold_left_s (fun (ctxt, bl, ids, acc) (k, v) ->
          consume_gas ctxt Love_gas.Cost_of.cycle >>=? fun ctxt ->
          aux ctxt fresh ids acc k >>=? fun (ctxt, k, ids, acc) ->
          aux ctxt fresh ids acc v >>|? fun (ctxt, v, ids, acc) ->
          (ctxt, (k, v) :: bl, ids, acc)
        ) (ctxt, [], ids, acc) (ValueMap.bindings vm)
        >>|? fun (ctxt, bl, ids, acc) ->
        let vm = Utils.bindings_to_map ValueMap.add ValueMap.empty bl in
        (ctxt, VMap vm, ids, acc)

    | VClosure ({ call_env = { values; _ } as call_env; lambda }) ->
        fold_left_s (fun (ctxt, vcl, ids, acc) (id, vc) ->
          consume_gas ctxt Love_gas.Cost_of.cycle >>=? fun ctxt ->
          match vc with
          | Local v ->
              aux ctxt fresh ids acc v >>|? fun (ctxt, v, ids, acc) ->
              (ctxt, (id, Local v) :: vcl, ids, acc)
          | Global (Inlined (_, _)) (* no big_map at top-level *)
          | Global (Pointer _) -> return (ctxt, vcl, ids, acc)
        ) (ctxt, [], ids, acc) values
        >>|? fun (ctxt, values, ids, acc) ->
        let v = VClosure { call_env = { call_env with values }; lambda } in
        (ctxt, v, ids, acc)

    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VNone | VSet _ | VDun _ | VKey _ | VKeyHash _ | VSignature _
    | VTimestamp _ | VAddress _ | VContractInstance _
    | VEntryPoint _ | VView _ | VOperation _ ->
        return (ctxt, v, ids, acc)

    | VPackedStructure _c -> (* Can not contain bigmaps *)
        return (ctxt, v, ids, acc)
  in
  aux ctxt fresh ids acc v >>|? fun (ctxt, v, ids, acc) ->
  (ctxt, Value.rec_closure v, ids, acc)

let collect_big_maps ctxt v =
  let open Value in
  let open Collections in
  let rec aux ctxt v acc =
    match Value.unrec_closure v with
    | VBigMap { id = Some id; _ } ->
        Gas.consume ctxt.actxt Love_gas.Cost_of.cycle >>? fun actxt ->
        ok (ZSet.add id acc, { ctxt with actxt })

    | VSome v ->
        aux ctxt v acc

    | VTuple vl | VList vl | VConstr (_, vl) | VPrimitive (_, vl) ->
        List.fold_left (fun acc v ->
          acc >>? fun (acc, ctxt) -> aux ctxt v acc
        ) (ok (acc, ctxt)) vl

    | VRecord fvl ->
        List.fold_left (fun acc (_, v) ->
          acc >>? fun (acc, ctxt) -> aux ctxt v acc
        ) (ok (acc, ctxt)) fvl

    | VMap vm -> (* Key should not contain bigmaps *)
        List.fold_left (fun acc (k, v) ->
          acc >>? fun (acc, ctxt) ->
          aux ctxt k acc >>? fun (acc, ctxt) ->
          aux ctxt v acc
        ) (ok (acc, ctxt)) (ValueMap.bindings vm)

    | VClosure { call_env = { values; _ }; _ } ->
        List.fold_left (fun acc (_id, vc) ->
          acc >>? fun (acc, ctxt) ->
          match vc with
          | Local v -> aux ctxt v acc
          | Global (Inlined (_, _)) (* no big_map at top-level *)
          | Global (Pointer _) -> ok (acc, ctxt)
        ) (ok (acc, ctxt)) values

    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VNone | VSet _ | VBigMap _ | VDun _ | VKey _ | VKeyHash _
    | VSignature _ | VTimestamp _ | VAddress _ | VContractInstance _
    | VEntryPoint _ | VView _ | VOperation _ ->
        ok (acc, ctxt)

    | VPackedStructure _c -> (* Can not contain bigmaps *)
        ok (acc, ctxt)
  in
  Lwt.return (aux ctxt v ZSet.empty)

let extract_big_map_diff ctxt ~temporary ~to_duplicate ~to_update v =
  let open Collections in
  let to_duplicate = ZSet.diff to_duplicate to_update in
  let fresh = if temporary
    then (fun ctxt ->
        Big_map.fresh_temporary ctxt.actxt
        |> fun (actxt, id) -> return ({ ctxt with actxt }, id))
    else (fun ctxt ->
        Big_map.fresh ctxt.actxt
        >>|? fun (actxt, id) -> { ctxt with actxt }, id) in
  extract_big_map_updates ctxt fresh to_duplicate [] v
  >>=? fun (ctxt, v, alive, diffs) ->
  let diffs = if temporary then diffs else
      let dead = ZSet.diff to_update alive in
      ZSet.fold (fun id acc -> Op.Clear id :: acc) dead [] :: diffs in
  match diffs with
  | [] -> return (v, None, ctxt)
  | diffs -> return (v, Some (List.flatten diffs (* do not reverse *)), ctxt)


let contract_at ctxt env a ct =
  Love_context.get_script ctxt a >>=? fun (ctxt, script) ->
  match script with
    | None -> return (ctxt, VNone)
    | Some (code, _storage) ->
      begin match ct with
        | Love_type.StructType sr -> sig_of_sigref ctxt env sr
        | ContractInstance cn ->
            Love_env.find_struct_opt ctxt env cn >>|? fun (ctxt, res) ->
            match res with
            | Some (_path, s) -> ctxt, sig_of_structure ~only_typedefs:true s
            | None -> raise (InvariantBroken "Structure not found")
      end >>=? fun (ctxt, sg) ->
      let deps = match code.kind with
        | Love_type.Module -> []
        | Love_type.Contract l -> l in
      Love_typechecker.extract_deps None ctxt
        (Love_tenv.empty code.kind () ()) deps
      >>|? fun (ctxt, deps_env) ->
      let cs = sig_of_structure ~only_typedefs:false code in
      let tenv = Love_tenv.contract_sig_to_env None () cs deps_env in
      let find_sig n =
        match Love_tenv.find_signature n tenv with
        | None -> raise (InvariantBroken ("Signature not found (at)"))
        | Some r -> Love_tenv.env_to_contract_sig r.result
      in
      match Love_type.sub_contract find_sig sg cs with
      | Ok -> (ctxt, VSome (VContractInstance (sg, a)))
      | TypeError _
      | TypeDefError _
      | SigContentError _
      | Other _ -> (ctxt, VNone)


let eval_prim apply_value ctxt env prim args =
  let open Collections in
  let module C = Compare in
  let module G = Love_gas.Cost_of in
  match prim, (List.rev args) with

  (* Comparisons *) (* May raise Uncomparable *)
  | PCompare, [v1; v2] -> return (ctxt, VInt (Z.of_int (Value.compare v1 v2)))
  | PEq,      [v1; v2] -> return (ctxt, VBool C.Int.(Value.compare v1 v2 = 0))
  | PNe,      [v1; v2] -> return (ctxt, VBool C.Int.(Value.compare v1 v2 <> 0))
  | PLt,      [v1; v2] -> return (ctxt, VBool C.Int.(Value.compare v1 v2 < 0))
  | PLe,      [v1; v2] -> return (ctxt, VBool C.Int.(Value.compare v1 v2 <= 0))
  | PGt,      [v1; v2] -> return (ctxt, VBool C.Int.(Value.compare v1 v2 > 0))
  | PGe,      [v1; v2] -> return (ctxt, VBool C.Int.(Value.compare v1 v2 >= 0))

  (* Arithmetic on Integers *)
  | PIAdd, [VInt i1; VInt i2] ->
      consume_gas ctxt (G.add i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.add i1 i2))
  | PISub, [VInt i1; VInt i2] ->
      consume_gas ctxt (G.sub i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.sub i1 i2))
  | PIMul, [VInt i1; VInt i2] ->
      consume_gas ctxt (G.mul i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.mul i1 i2))
  | PIDiv, [VInt i1; VInt i2] ->
      consume_gas ctxt (G.div i1 i2) >>|? fun ctxt ->
      begin try
          let q, r = Z.ediv_rem i1 i2 in
          (ctxt, VSome (VTuple [VInt q; VNat r]))
        with Division_by_zero -> (ctxt, VNone)
      end
  | PINeg, [VInt i] ->
      consume_gas ctxt (G.neg i) >>|? fun ctxt ->
      (ctxt, VInt (Z.neg i))
  | PIAbs, [VInt i] ->
      consume_gas ctxt (G.abs i) >>|? fun ctxt ->
      (ctxt, VInt (Z.abs i))

  (* Arithmetic on Naturals *)
  | PNAdd, [VNat i1; VNat i2] ->
      consume_gas ctxt (G.add i1 i2) >>|? fun ctxt ->
      (ctxt, VNat (Z.add i1 i2))
  | PNSub, [VNat i1; VNat i2] ->
      consume_gas ctxt (G.sub i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.sub i1 i2))
  | PNMul, [VNat i1; VNat i2] ->
      consume_gas ctxt (G.mul i1 i2) >>|? fun ctxt ->
      (ctxt, VNat (Z.mul i1 i2))
  | PNDiv, [VNat i1; VNat i2] ->
      consume_gas ctxt (G.div i1 i2) >>|? fun ctxt ->
      begin try
          let q, r = Z.ediv_rem i1 i2 in
          (ctxt, VSome (VTuple [VNat q; VNat r]))
        with Division_by_zero -> (ctxt, VNone)
      end
  | PNNeg, [VNat i] ->
      consume_gas ctxt (G.neg i) >>|? fun ctxt ->
      (ctxt, VInt (Z.neg i))
  | PNAbs, [VInt i] ->
      consume_gas ctxt (G.abs i) >>|? fun ctxt ->
      (ctxt, VNat (Z.abs i))

  (* Arithmetic on Integers/Naturals *)
  | PNIAdd, [VNat i1; VInt i2]
  | PINAdd, [VInt i1; VNat i2] ->
      consume_gas ctxt (G.add i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.add i1 i2))
  | PNISub, [VNat i1; VInt i2]
  | PINSub, [VInt i1; VNat i2] ->
      consume_gas ctxt (G.sub i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.sub i1 i2))
  | PNIMul, [VNat i1; VInt i2]
  | PINMul, [VInt i1; VNat i2] ->
      consume_gas ctxt (G.mul i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.mul i1 i2))
  | PNIDiv, [VNat i1; VInt i2]
  | PINDiv, [VInt i1; VNat i2] ->
      consume_gas ctxt (G.div i1 i2) >>|? fun ctxt ->
      begin try
          let q, r = Z.ediv_rem i1 i2 in
          (ctxt, VSome (VTuple [VInt q; VNat r]))
        with Division_by_zero -> (ctxt, VNone)
      end

  (* Arithmetic on Duns *)
  | PDAdd, [VDun d1; VDun d2] ->
      consume_gas ctxt (G.int64_op) >>=? fun ctxt ->
      Lwt.return (Tez.(d1 +? d2) >|? fun res -> (ctxt, VDun res))
  | PDSub, [VDun d1; VDun d2] ->
      consume_gas ctxt (G.int64_op) >>=? fun ctxt ->
      if C.Int.(Tez.compare d1 d2 < 0) then return (ctxt, VNone)
      else Lwt.return (Tez.(d1 -? d2) >|? fun res -> (ctxt, VSome (VDun res)))
  | PDIMul, [VDun d; VInt i]
  | PIDMul, [VInt i; VDun d] -> (* May raise Z.Overflow *)
      consume_gas ctxt (G.int64_op) >>=? fun ctxt ->
      if C.Int.(Z.compare i Z.zero < 0) then return (ctxt, VNone)
      else Lwt.return (Tez.(d *? (Z.to_int64 i))
                       >|? fun res -> (ctxt, VSome (VDun res)))
  | PDNMul, [VDun d; VNat i]
  | PNDMul, [VNat i; VDun d] -> (* May raise Z.Overflow *)
      consume_gas ctxt (G.int64_op) >>=? fun ctxt ->
      Lwt.return (Tez.(d *? (Z.to_int64 i)) >|? fun res -> (ctxt, VDun res))
  | PDIDiv, [VDun d; VInt i]
  | PDNDiv, [VDun d; VNat i] -> (* May raise Z.Overflow *)
      consume_gas ctxt (G.int64_op) >>|? fun ctxt ->
      if C.Int.(Z.compare i Z.zero < 0) then (ctxt, VNone) else
      let d, i = Tez.to_mutez d, Z.to_int64 i in
      begin try
          let q, r = Int64.div d i, Int64.rem d i in
          begin match Tez.of_mutez q, Tez.of_mutez r with
            | None, _ | _, None -> (ctxt, VNone)
            | Some q, Some r -> (ctxt, VSome (VTuple [VDun q; VDun r]))
          end
        with Division_by_zero -> (ctxt, VNone)
      end
  | PDDiv, [VDun d1; VDun d2] ->
      consume_gas ctxt (G.int64_op) >>|? fun ctxt ->
      let d1, d2 = Tez.to_mutez d1, Tez.to_mutez d2 in
      begin try
          let q, r = Int64.div d1 d2, Int64.rem d1 d2 in
          begin match Tez.of_mutez r with
            | None -> (ctxt, VNone)
            | Some r -> (ctxt, VSome (VTuple [VInt (Z.of_int64 q); VDun r]))
          end
        with Division_by_zero -> (ctxt, VNone)
      end

  (* Arithmetic on Timestamps *)
  | PTIAdd, [VTimestamp t; VInt i]
  | PITAdd, [VInt i; VTimestamp t]
  | PTNAdd, [VTimestamp t; VNat i]
  | PNTAdd, [VNat i; VTimestamp t] ->
      consume_gas ctxt (G.add_timestamp t i) >>|? fun ctxt ->
      let i = Script_int.of_zint i in
      (ctxt, VTimestamp (Script_timestamp.add_delta t i))
  | PTISub, [VTimestamp t; VInt i]
  | PTNSub, [VTimestamp t; VNat i] ->
      consume_gas ctxt (G.sub_timestamp t i) >>|? fun ctxt ->
      let i = Script_int.of_zint i in
      (ctxt, VTimestamp (Script_timestamp.sub_delta t i))
  | PTSub, [VTimestamp t1; VTimestamp t2] ->
      consume_gas ctxt (G.diff_timestamps t1 t2) >>|? fun ctxt ->
      let i = Script_timestamp.diff t1 t2 in
      (ctxt, VInt (Script_int.to_zint i))

  (* Boolean operators *) (* TODO : lazy and/or *)
  | PBAnd, [VBool b1; VBool b2] ->
      consume_gas ctxt (G.bool_binop b1 b2) >>|? fun ctxt ->
      (ctxt, VBool (b1 && b2))
  | PBOr,  [VBool b1; VBool b2] ->
      consume_gas ctxt (G.bool_binop b1 b2) >>|? fun ctxt ->
      (ctxt, VBool (b1 || b2))
  | PBXor, [VBool b1; VBool b2] ->
      consume_gas ctxt (G.bool_binop b1 b2) >>|? fun ctxt ->
      (ctxt, VBool C.Bool.(b1 <> b2))
  | PBNot, [VBool b]            ->
      consume_gas ctxt (G.bool_unop b) >>|? fun ctxt ->
      (ctxt, VBool (not b))

  (* Bitwise operators (Integer) *)
  | PIAnd, [VInt i1; VInt i2] ->
      consume_gas ctxt (G.logand i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.logand i1 i2))
  | PIOr,  [VInt i1; VInt i2] ->
      consume_gas ctxt (G.logor i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.logor i1 i2))
  | PIXor, [VInt i1; VInt i2] ->
      consume_gas ctxt (G.logor i1 i2) >>|? fun ctxt ->
      (ctxt, VInt (Z.logxor i1 i2))
  | PINot, [VInt i]           ->
      consume_gas ctxt (G.lognot i) >>|? fun ctxt ->
      (ctxt, VInt (Z.lognot i))
  | PILsl, [VInt i1; VInt i2] -> (* If negative : shift in opposite direction *)
      consume_gas ctxt (G.shift_left i1 i2) >>|? fun ctxt ->
      let i2 = Z.to_int i2 in (* May raise Z.Overflow *)
      if C.Int.(i2 >= 0) then (ctxt, VInt (Z.shift_left i1 i2))
      else (ctxt, VInt (Z.shift_right i1 (abs i2)))
  | PILsr, [VInt i1; VInt i2] ->
      consume_gas ctxt (G.shift_right i1 i2) >>|? fun ctxt ->
      let i2 = Z.to_int i2 in (* May raise Z.Overflow *)
      if C.Int.(i2 >= 0) then (ctxt, VInt (Z.shift_right i1 i2))
      else (ctxt, VInt (Z.shift_left i1 (abs i2)))

  (* Bitwise operators (Natural) *)
  | PNAnd, [VNat i1; VNat i2] ->
      consume_gas ctxt (G.logand i1 i2) >>|? fun ctxt ->
      (ctxt, VNat (Z.logand i1 i2))
  | PNOr,  [VNat i1; VNat i2] ->
      consume_gas ctxt (G.logor i1 i2) >>|? fun ctxt ->
      (ctxt, VNat (Z.logor i1 i2))
  | PNXor, [VNat i1; VNat i2] ->
      consume_gas ctxt (G.logor i1 i2) >>|? fun ctxt ->
      (ctxt, VNat (Z.logxor i1 i2))
  | PNLsl, [VNat i1; VNat i2] ->
      consume_gas ctxt (G.shift_left i1 i2) >>|? fun ctxt ->
      let i2 = Z.to_int i2 in (* May raise Z.Overflow *)
      (ctxt, VNat (Z.shift_left i1 i2))
  | PNLsr, [VNat i1; VNat i2] ->
      consume_gas ctxt (G.shift_right i1 i2) >>|? fun ctxt ->
      let i2 = Z.to_int i2 in (* May raise Z.Overflow *)
      (ctxt, VNat (Z.shift_right i1 i2)) (* i1 >= 0, hence result >= 0 *)

  (* List primitives *)
  | PLLength, [VList l]    -> return (ctxt, VNat (Z.of_int (List.length l)))
  | PLCons,   [v; VList l] -> return (ctxt, VList (v :: l))
  | PLRev,    [VList l]    -> return (ctxt, VList (List.rev l))
  | PLConcat, [VList l]->
      return (ctxt, VList (List.concat (List.map (function
          | VList l -> l
          | _ -> raise (InvariantBroken "Invalid List.concat argument")) l)))

  | PLIter, [f; VList l] ->
      fold_left_s (fun ctxt v ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [v])
          >>|? fun (ctxt, _v') -> ctxt) ctxt l
      >>|? fun ctxt -> (ctxt, VUnit)

  | PLMap, [f; VList l] ->
      fold_left_s (fun (ctxt, l) v ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [v])
          >>|? fun (ctxt, v') -> (ctxt, v' :: l)) (ctxt, []) l
      >>|? fun (ctxt, l) -> (ctxt, VList (List.rev l))

  | PLFold, [f; VList l; a] ->
      fold_left_s (fun (ctxt, a) v ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [v; a])) (ctxt, a) l

  | PLMapFold, [f; VList l; a] ->
      fold_left_s (fun (ctxt, (l, a)) v ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [v; a]) >>|? function
          | ctxt, VTuple [v'; a'] -> (ctxt, (v' :: l, a'))
          | _ -> raise (InvariantBroken "Invalid List.map_fold function")
        ) (ctxt, ([], a)) l
      >>|? fun (ctxt, (l, a)) -> (ctxt, VTuple [VList (List.rev l); a])

  (* Set primitives *)
  | PSCard,   [VSet s]  -> return (ctxt, VNat (Z.of_int (ValueSet.cardinal s)))
  | PSEmpty,  []        -> return (ctxt, VSet (ValueSet.empty))
  | PSAdd,    [c; VSet s] -> return (ctxt, VSet (ValueSet.add c s))
  | PSRemove, [c; VSet s] -> return (ctxt, VSet (ValueSet.remove c s))
  | PSMem,    [c; VSet s] -> return (ctxt, VBool (ValueSet.mem c s))

  | PSIter, [f; VSet s] ->
      fold_left_s (fun ctxt v ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [v])
          >>|? fun (ctxt, _v') -> ctxt) ctxt (ValueSet.elements s)
      >>|? fun ctxt -> (ctxt, VUnit)

  | PSMap, [f; VSet s] ->
      fold_left_s (fun (ctxt, s) v ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [v])
          >>|? fun (ctxt, v') -> (ctxt, ValueSet.add v' s)
        ) (ctxt, ValueSet.empty) (ValueSet.elements s)
      >>|? fun (ctxt, s) -> (ctxt, VSet s)

  | PSFold, [f; VSet s; a] ->
      fold_left_s (fun (ctxt, a) v ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [v; a])
        ) (ctxt, a) (ValueSet.elements s)
      >>|? fun (ctxt, a) -> (ctxt, a)

  | PSMapFold, [f; VSet s; a] ->
      fold_left_s (fun (ctxt, (s, a)) v ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [v; a]) >>|? function
          | ctxt, VTuple [v'; a'] -> (ctxt, (ValueSet.add v' s, a'))
          | _ -> raise (InvariantBroken "Invalid Set.map_fold function")
        ) (ctxt, (ValueSet.empty, a)) (ValueSet.elements s)
      >>|? fun (ctxt, (s, a)) -> (ctxt, VTuple [VSet s; a])

  (* Map primitives *)
  | PMCard,   [VMap m]  -> return (ctxt, VNat (Z.of_int (ValueMap.cardinal m)))
  | PMEmpty,  []        -> return (ctxt, VMap (ValueMap.empty))
  | PMAdd,    [k; v; VMap m] -> return (ctxt, VMap (ValueMap.add k v m))
  | PMRemove, [k; VMap m]    -> return (ctxt, VMap (ValueMap.remove k m))
  | PMMem,    [k; VMap m]    -> return (ctxt, VBool (ValueMap.mem k m))

  | PMFind, [k; VMap m] ->
      begin match ValueMap.find_opt k m with
        | None -> return (ctxt, VNone)
        | Some v -> return (ctxt, VSome v)
      end

  | PMIter, [f; VMap m] ->
      fold_left_s (fun ctxt (k, v) ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [k; v])
          >>|? fun (ctxt, _v') -> ctxt) ctxt (ValueMap.bindings m)
      >>|? fun ctxt -> (ctxt, VUnit)

  | PMMap, [f; VMap m] ->
      fold_left_s (fun (ctxt, m) (k, v) ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [k; v])
          >>|? fun (ctxt, v') -> (ctxt, ValueMap.add k v' m)
        ) (ctxt, ValueMap.empty) (ValueMap.bindings m)
      >>|? fun (ctxt, m) -> (ctxt, VMap m)

  | PMFold, [f; VMap m; a] ->
      fold_left_s (fun (ctxt, a) (k, v) ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [k; v; a])
        ) (ctxt, a) (ValueMap.bindings m)
      >>|? fun (ctxt, a) -> (ctxt, a)

  | PMMapFold, [f; VMap m; a] ->
      fold_left_s (fun (ctxt, (m, a)) (k, v) ->
          consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
          apply_value ctxt env f (Val [k; v; a]) >>|? function
          | ctxt, VTuple [v'; a'] -> (ctxt, (ValueMap.add k v' m, a'))
          | _ -> raise (InvariantBroken "Invalid Map.map_fold function")
        ) (ctxt, (ValueMap.empty, a)) (ValueMap.bindings m)
      >>|? fun (ctxt, (m, a)) -> (ctxt, VTuple [VMap m; a])

  (* Bigmap primitives *)
  | PBMEmpty (tk, tv), [] ->
      consume_gas ctxt (G.big_map_empty tk tv) >>=? fun ctxt ->
      Love_translator.make_type_absolute ctxt env tk >>=? fun (ctxt, tk) ->
      Love_translator.make_type_absolute ctxt env tv >>|? fun (ctxt, tv) ->
      ctxt, (VBigMap (empty_big_map tk tv))
  | PBMAdd, [k; v; VBigMap bm] ->
      consume_gas ctxt (G.big_map_update k (Some v) ()) >>=? fun ctxt ->
      big_map_add ctxt bm k v >>|? fun (ctxt, bm) -> ctxt, VBigMap bm
  | PBMRemove, [k; VBigMap bm] ->
      consume_gas ctxt (G.big_map_update k None ()) >>=? fun ctxt ->
      big_map_del ctxt bm k >>|? fun (ctxt, bm) -> ctxt, VBigMap bm
  | PBMMem, [k; VBigMap bm] ->
      consume_gas ctxt (G.big_map_mem k ()) >>=? fun ctxt ->
      big_map_mem ctxt bm k >>|? fun (ctxt, res) -> ctxt, VBool res
  | PBMFind, [k; VBigMap bm] ->
      consume_gas ctxt (G.big_map_get k ()) >>=? fun ctxt ->
      big_map_get ctxt bm k

  (* Loop primitive *)
  | PLLoop, [f; v] ->
      let rec loop ctxt v =
        consume_gas ctxt G.loop_cycle >>=? fun ctxt ->
        apply_value ctxt env f (Val [v]) >>=? function
        | ctxt, VTuple [VBool true; v'] -> loop ctxt v'
        | ctxt, VTuple [VBool false; v'] -> return (ctxt, v')
        | _ -> raise (InvariantBroken "Invalid Loop.loop function")
      in
      loop ctxt v

  (* String primitives *)
  | PSLength, [VString s] -> return (ctxt, VNat (Z.of_int (String.length s)))

  | PSConcat, [VList l] ->
      map_s (function
          | VString s -> return s
          | _ -> raise (InvariantBroken "Bad String.concat argument")) l
      >>|? fun l -> (ctxt, VString (String.concat "" l))

  | PSSlice, [VNat i1; VNat i2; VString s] -> (* May raise Z.Overflow *)
      let res = try VSome (VString (String.sub s (Z.to_int i1) (Z.to_int i2)))
        with Invalid_argument _ -> VNone in
      return (ctxt, res)

  (* Bytes primitives *)
  | PBLength, [VBytes b] -> return (ctxt, VNat (Z.of_int (MBytes.length b)))

  | PBConcat, [VList l] ->
      map_s (function
          | VBytes b -> return b
          | _ -> raise (InvariantBroken "Bad Bytes.concat argument")) l
      >>|? fun l -> (ctxt, VBytes (MBytes.concat "" l))

  | PBSlice, [VNat i1; VNat i2; VBytes b] -> (* May raise Z.Overflow *)
      let res = try VSome (VBytes (MBytes.sub b (Z.to_int i1) (Z.to_int i2)))
        with Invalid_argument _ -> VNone in
      return (ctxt, res)

  | PBPack, [v] ->
      begin match Data_encoding.Binary.to_bytes
                    Love_encoding.Value.encoding v with
      | None -> raise (InvariantBroken "Can't encode value")
      | Some b ->
          (* 5 = Packed value, 1 = encoding version *)
          let b = MBytes.concat ""
              [ MBytes.of_string "\005" ; MBytes.of_string "\001" ; b ] in
          return (ctxt, VBytes b)
      end

  | PBUnpack ty, [VBytes b] ->
      if Compare.Int.(MBytes.length b >= 2) &&
         Compare.Int.(MBytes.get_uint8 b 0 = 0x05) then
        if Compare.Int.(MBytes.get_uint8 b 1 = 0x01) then
          let b = MBytes.sub b 2 (MBytes.length b - 2) in
          match Data_encoding.Binary.of_bytes
                  Love_encoding.Value.encoding b with
          | None -> return (ctxt, VNone)
          | Some v ->
              try Love_typechecker.typecheck_value
                    (Love_tenv.empty (Contract []) () ()) ctxt
                    { lettypes = []; body = ty } v >>=? fun ctxt ->
                return (ctxt, VSome v)
              with _ -> return (ctxt, VNone)
        else raise (InvariantBroken "Unsupported encoding version")
      else return (ctxt, VNone)

  | PBHash, [VBytes b] ->
      hash_data ctxt (VBytes b) >>|? fun (ctxt, res) ->
      (ctxt, VBytes (Script_expr_hash.to_bytes res))

  (* Cryptographic operations *)
  | PCBlake2b, [VBytes b] -> return (ctxt, VBytes (Raw_hashes.blake2b b))
  | PCSha256,  [VBytes b] -> return (ctxt, VBytes (Raw_hashes.sha256 b))
  | PCSha512,  [VBytes b] -> return (ctxt, VBytes (Raw_hashes.sha512 b))
  | PCHashKey, [VKey key] ->
      return (ctxt, VKeyHash (Signature.Public_key.hash key))
  | PCCheck, [VKey key; VSignature sign; VBytes msg] ->
      return (ctxt, VBool (Signature.check key sign msg))

  (* Contract interactions *)
  | PCAddress, [VContractInstance (_, a)] ->
      consume_gas ctxt G.address >>=? fun ctxt ->
      return (ctxt, VAddress a)

  | PCSelf ct, [VUnit] ->
      consume_gas ctxt G.self >>=? fun ctxt ->
      contract_at ctxt env ctxt.self ct

  | PCAt ct, [VAddress a] ->
      consume_gas ctxt G.contract >>=? fun ctxt ->
      contract_at ctxt env a ct

  | PCCall,
    [VEntryPoint (destination, entrypoint); VDun amount; arg] ->
      consume_gas ctxt G.transfer >>=? fun ctxt ->
      let arg = Love_translator.inline_value ctxt arg in
      collect_big_maps ctxt arg >>=? fun (to_duplicate, ctxt) ->
      extract_big_map_diff ctxt arg ~to_duplicate ~to_update:ZSet.empty
        ~temporary:true >>=? fun (arg, big_map_diff, ctxt) ->
      let parameters = Some arg in
      let operation = Op.Transaction {
          amount; parameters; entrypoint; destination } in
      Lwt.return @@ fresh_internal_nonce ctxt.actxt >>|? fun (actxt, nonce) ->
      let op = Op.{ source = ctxt.self; operation; nonce = nonce } in
      ({ ctxt with actxt }, VOperation (op, big_map_diff))
  (* Note : can only fail if out of gas or more than 65535 internal ops *)

  | PCView, [VView (destination, view); arg] ->
      consume_gas ctxt G.view >>=? fun ctxt ->
      Love_context.get_script ctxt destination >>=? fun (ctxt, script) ->
      begin match script with
        | None -> raise (InvariantBroken "Bad Contract.view contract")
        | Some (code, storage) ->
            match List.assoc_opt view code.content with
            | Some (VView { vview_code = v; _ }) ->
                let fct = match v with
                  | VClosure _ as v -> v
                  | _ -> raise BadView in
                apply_value ctxt env fct (Val [storage; arg])
            | _ -> raise BadView
      end

  | PCCreate, [deleg_opt; VDun credit; VPackedStructure (p, c); init_arg] ->
      consume_gas ctxt G.create_contract >>=? fun ctxt ->
      if Love_value.LiveStructure.is_module c.root_struct then
        raise (InvariantBroken "Contract.create requires a contract structure");
      Contract.fresh_contract_from_current_nonce ctxt.actxt
      >>=? fun (actxt, contract) ->
      let ctxt = { ctxt with actxt } in
      let to_path = Ident.create_id (Contract.to_b58check contract) in
      let code = Love_translator.rebase_contract ctxt ~from_path:p ~to_path c in
      let init_storage = init_arg in
      let init_storage = Love_translator.inline_value ctxt init_storage in
      collect_big_maps ctxt init_storage >>=? fun (to_duplicate, ctxt) ->
      extract_big_map_diff ctxt init_storage ~to_duplicate ~to_update:ZSet.empty
        ~temporary:true >>=? fun (init_storage, big_map_diff, ctxt) ->
      let delegate = match deleg_opt with
        | VNone -> None
        | VSome (VKeyHash kh) -> Some kh
        | _ -> raise (InvariantBroken "Bad Contract.create argument") in
      let operation = Op.Origination {
          delegate; script = (init_storage, code);
          credit; preorigination = Some contract } in
      Lwt.return @@ fresh_internal_nonce actxt >>|? fun (actxt, nonce) ->
      let op = Op.{ source = ctxt.self; operation; nonce } in
      ({ ctxt with actxt },
       VTuple [VOperation (op, big_map_diff); VAddress contract])
  (* Note : can fail if out of gas or origination nonce
     not initialized or more then 65535 internal ops *)

  | PCSetDeleg, [deleg_opt] ->
      consume_gas ctxt G.set_delegate >>=? fun ctxt ->
      let delegate = match deleg_opt with
        | VNone -> None
        | VSome (VKeyHash kh) -> Some kh
        | _ -> raise (InvariantBroken "Bad Contract.set_delegate argument") in
      let operation = Op.Delegation delegate in
      Lwt.return @@ fresh_internal_nonce ctxt.actxt >>|? fun (actxt, nonce) ->
      let op = Op.{ source = ctxt.self; operation; nonce = nonce } in
      ({ ctxt with actxt }, VOperation (op, None))
  (* Note : can only fail if out of gas or more than 65535 internal ops *)

  (* Account interactions *)
  | PADefault, [VKeyHash kh] ->
      consume_gas ctxt G.implicit_account >>=? fun ctxt ->
      return (ctxt, VContractInstance (Love_type.unit_contract_sig,
                                       Contract.implicit_contract kh))

  | PATransfer, [VAddress destination; VDun amount] ->
      consume_gas ctxt G.transfer >>=? fun ctxt ->
      let operation = Op.Transaction {
          amount; parameters = None; entrypoint = "default"; destination } in
      Lwt.return @@ fresh_internal_nonce ctxt.actxt >>|? fun (actxt, nonce) ->
      let op = Op.{ source = ctxt.self; operation; nonce = nonce } in
      ({ ctxt with actxt }, VOperation (op, None))

  | PABalanceOf, [VAddress a] ->
      consume_gas ctxt G.balance >>=? fun ctxt ->
      Contract.get_balance ctxt.actxt a >>|? fun balance ->
      (ctxt, VDun balance)

  | PAGetInfo, [VAddress target] ->
      consume_gas ctxt G.getinfo >>=? fun ctxt ->
      begin
        match Contract.is_implicit target with
        | None -> return None
        | Some delegate ->
            Roll.Delegate.get_maxrolls ctxt.actxt delegate
      end >>=? fun maxrolls ->
      let maxrolls = match maxrolls with
        | None -> VNone
        | Some i -> VSome (VInt (Z.abs (Z.of_int i))) in
      Contract.get_delegation ctxt.actxt target >>=? fun deleg ->
      Contract.get_admin ctxt.actxt target >>=? fun admin ->
      let admin = match admin with
        | Some a -> VSome (VAddress a)
        | None -> VNone in
      Contract.get_whitelist ctxt.actxt target >>= fun wl ->
      let wl = List.map (fun v -> VAddress v) wl in
      return (ctxt, VTuple [maxrolls; VBool deleg; admin; VList wl])

  | PAManage, [VAddress target; maxrolls_opt; VBool deleg;
               admin_opt; VList white_list] ->
      consume_gas ctxt G.manage >>=? fun ctxt ->
      let maxrolls = match maxrolls_opt with
        | VNone -> Some (None)
        | VSome (VInt maxrolls) -> Some (Some (Z.to_int maxrolls))
        | _ -> raise (InvariantBroken "Invalid maxrolls: not an int")
      in
      let admin = match admin_opt with
        | VNone -> Some None
        | VSome (VAddress a) -> Some (Some a)
        | _ -> raise (InvariantBroken "Invalid admin: not an address")
      in
      let white_list = Some (List.map (function
          | VAddress a -> a
          | _ -> raise (InvariantBroken
                          "Invalid white list: not an address list")
        ) white_list)
      in
      let delegation = Some deleg in
      let target = match Contract.is_implicit target with
        | Some target_pkh -> Some target_pkh
        | None -> None
      in
      let operation = Op.Dune_manage_account {
          target; maxrolls; admin; white_list; delegation } in
      Lwt.return (fresh_internal_nonce ctxt.actxt)
      >>=? fun (actxt, nonce) ->
      let op = Op.{ source = ctxt.self; operation; nonce = nonce } in
      return ({ ctxt with actxt }, VSome (VOperation (op, None)))

(*
     (* First, check that we are allowed to do that *)
     begin Contract.get_admin ctxt.actxt target >>=? function
       | None -> return target
       | Some admin -> return admin
     end >>=? fun current_admin ->
     if Contract.(current_admin = ctxt.self) then
       begin match Contract.is_implicit target with
         | Some _target_pkh ->
            let maxrolls = match maxrolls_opt with
              | VNone -> Some (None)
              | VSome (VInt maxrolls) -> Some (Some (Z.to_int maxrolls))
              | _ -> raise (InvariantBroken "Invalid maxrolls: not an int")
            in
            let admin = match admin_opt with
               | VNone -> Some None
               | VSome (VAddress a) -> Some (Some a)
               | _ -> raise (InvariantBroken "Invalid admin: not an address")
            in
            let white_list = Some (List.map (function
                | VAddress a -> a
                | _ -> raise (InvariantBroken
                                "Invalid white list: not an address list")
              ) white_list)
            in
            let delegation = Some deleg in
            let operation = Op.Dune_manage_account {
                target; maxrolls; admin; white_list; delegation } in
            Lwt.return (fresh_internal_nonce ctxt.actxt)
            >>=? fun (actxt, nonce) ->
            let op = Op.{ source = ctxt.self; operation; nonce = nonce } in
            return ({ ctxt with actxt }, VSome (VOperation (op, None)))
         | None -> return (ctxt, VNone)
       end
     else
       consume_gas ctxt (G.raiseexn (List.length args)) >>=? fun ctxt ->
       raise (UserException (ctxt, (
           RFail TString, [VString "Manage_account by not admin"]), None))
*)

  (* General information *)
  | PCGas, [VUnit] ->
      consume_gas ctxt G.steps_to_quota >>=? fun ctxt ->
      let steps = match Gas.level ctxt.actxt with
        | Unaccounted -> Z.of_int 99999999
        | Limited { remaining } -> remaining
      in
      return (ctxt, VNat steps)
  | PCBalance, [VUnit] ->
      consume_gas ctxt G.balance >>=? fun ctxt ->
      Contract.get_balance ctxt.actxt ctxt.self >>|? fun balance ->
      (ctxt, VDun balance)
  | PCTime, [VUnit] ->
      consume_gas ctxt G.now >>=? fun ctxt ->
      return (ctxt, VTimestamp (Script_timestamp.now ctxt.actxt))
  | PCAmount, [VUnit] ->
      consume_gas ctxt G.amount >>=? fun ctxt ->
      return (ctxt, VDun ctxt.amount)
  | PCrSelf, [VUnit] ->
      consume_gas ctxt G.cself >>=? fun ctxt ->
      return (ctxt, VAddress ctxt.self)
  | PCSource, [VUnit] ->
      consume_gas ctxt G.source >>=? fun ctxt ->
      return (ctxt, VAddress ctxt.payer)
  | PCSender, [VUnit] ->
      consume_gas ctxt G.sender >>=? fun ctxt ->
      return (ctxt, VAddress ctxt.source)
  | PCLevel, [VUnit] ->
      consume_gas ctxt G.level >>=? fun ctxt ->
      let level = Level.current ctxt.actxt in
      let l = Raw_level.to_int32 level.level in
      return (ctxt, VNat (Z.of_int32 l))
  | PCCycle, [VUnit] ->
      consume_gas ctxt G.ccycle >>=? fun ctxt ->
      let level = Level.current ctxt.actxt in
      let l = Cycle.to_int32 level.cycle in
      return (ctxt, VNat (Z.of_int32 l))

  (* Conversion *)
  | PAddressOfKeyHash, [VKeyHash kh] ->
      consume_gas ctxt G.address_of_keyhash >>|? fun ctxt ->
      (ctxt, VAddress (Contract.implicit_contract kh))
  | PKeyHashOfAddress, [VAddress a] ->
      consume_gas ctxt G.keyhash_of_address >>|? fun ctxt ->
      begin match a with
        | Implicit a -> (ctxt, VSome (VKeyHash a))
        | Originated _ -> (ctxt, VNone)
      end

  | PGenericPrimitive p, _ ->
      let ( gas, f ) = generic_primitives.(p.prim_id) in
      consume_gas ctxt gas >>|? fun ctxt ->
      f apply_value ctxt env p args

  (* Exhaustiveness *)
  | (PCompare | PEq | PNe | PLt | PLe | PGt | PGe |
     PIAdd | PISub | PIMul | PIDiv | PINeg | PIAbs |
     PNAdd | PNSub | PNMul | PNDiv | PNNeg | PNAbs |
     PINAdd | PINSub | PINMul | PINDiv |
     PNIAdd | PNISub | PNIMul | PNIDiv |
     PDAdd | PDSub | PDDiv |
     PDIMul | PIDMul | PDIDiv |
     PDNMul | PNDMul | PDNDiv |
     PTSub |
     PTIAdd | PITAdd | PTISub |
     PTNAdd | PNTAdd | PTNSub |
     PBAnd | PBOr | PBXor | PBNot |
     PIAnd | PIOr | PIXor | PINot | PILsl | PILsr |
     PNAnd | PNOr | PNXor | PNLsl | PNLsr |
     PLLength | PLCons | PLRev | PLConcat |
     PLIter | PLMap | PLFold | PLMapFold |
     PSCard | PSEmpty | PSAdd | PSRemove | PSMem |
     PSIter | PSMap | PSFold | PSMapFold |
     PMCard | PMEmpty | PMAdd | PMRemove | PMMem | PMFind |
     PMIter | PMMap | PMFold | PMMapFold |
     PBMEmpty _ | PBMAdd | PBMRemove | PBMMem | PBMFind |
     PLLoop |
     PSLength | PSConcat | PSSlice |
     PBLength | PBConcat | PBSlice | PBPack | PBUnpack _ | PBHash |
     PCBlake2b | PCSha256 | PCSha512 | PCHashKey | PCCheck |
     PCAddress | PCSelf _ | PCAt _ | PCSetDeleg |
     PCCall | PCView | PCCreate |
     PATransfer | PADefault | PABalanceOf | PAGetInfo | PAManage |
     PCBalance | PCTime | PCAmount | PCGas |
     PCrSelf | PCSource | PCSender | PCLevel | PCCycle |
     PAddressOfKeyHash | PKeyHashOfAddress
    ), _ ->
      let pn = Love_primitive.name prim in
      let an = string_of_int (List.length args) in
      raise (InvariantBroken ("Love_primitive '" ^ pn
                              ^ "' called with the wrong number or kind of arguments (" ^ an ^ ")"))

let eval apply_value ctxt env prim args =
  let res = List.compare_length_with args (arity prim) in
  if Compare.Int.(res < 0) then
    return (ctxt, VPrimitive (prim, args))
  else if Compare.Int.(res > 0) then
    raise (InvariantBroken "Love_primitive called with too many arguments")
  else
    eval_prim apply_value ctxt env prim args
