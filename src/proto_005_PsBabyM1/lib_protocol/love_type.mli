(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Collections

(** The type traits *)
type traits = { tcomparable : bool }

(** Equality check on two traits *)
val equal_traits : traits -> traits -> bool

(** A default trait (uncomparable) *)
val default_trait : traits

(** Trait for comparable types *)
val comparable : traits

(** Type variables 'a are represented by their name and traits *)
type type_var = { tv_name : string; tv_traits : traits; }

(** Name and traits equality *)
val equal_type_var : type_var -> type_var -> bool

(** Type names are identifyers*)
type type_name = string Love_pervasives.Ident.t

(** Equality on type names *)
val equal_tname : type_name -> type_name -> bool

type structure_type_name = string Love_pervasives.Ident.t

type multiple_path =
    Local
  | AbsolutePath of string list
  | Multiple of multiple_path list

val equal_multiple_path : multiple_path -> multiple_path -> bool

val pp_multiple_path : Format.formatter -> multiple_path -> unit

type dependency = string * string

type dependencies = dependency list

type struct_kind =
    Module
  | Contract of dependencies

type recursive = Rec | NonRec

(** Type definitions. Can be an alias, a sum type or a record. *)
type typedef =
    Alias of {
      aparams : type_var list;
      atype : t;
    }
  | SumType of {
      sparams : type_var list;
      scons : (string * t list) list;
      srec : recursive;
    }
  | RecordType of {
      rparams : type_var list;
      rfields : (string * t) list;
      rrec : recursive
    }

(** Types in signature can be abstract (no access to definition),
    private (read only) or public. *)
and sig_type =
    SAbstract of type_var list
  | SPrivate of typedef
  | SPublic of typedef

and sig_content =
  | SType of sig_type
  | SException of t list
  | SEntry of t (* entry points parameter *)
  | SView of t * t (* parameter and result *)
  | SValue of t
  | SStructure of structure_type
  | SSignature of structure_sig

(** The signature of a contract or a module. *)
and structure_sig = {

  sig_kind    : struct_kind;

  sig_content : (string * sig_content) list;
  (** The signature content *)
}

(** The type of a structure can be a raw structure signature with every detail, or
    a signature defined by its name. *)
and structure_type =
    Anonymous of structure_sig
  | Named of structure_type_name

and contract_type =
  | StructType of structure_type             (* either a usual structure type *)
  | ContractInstance of structure_type_name  (* or an instance of a contract structure *)

(** Types. *)
and t =
    TUnit
  | TBool
  | TString
  | TBytes
  | TInt
  | TNat
  | TOption of t (** Some of t | None *)
  | TTuple of t list
  | TUser of type_name * t list  (** Types defined in the contract *)
  | TList of t
  | TSet of t
  | TMap of t * t
  | TBigMap of t * t
  | TDun
  | TKey
  | TKeyHash
  | TSignature
  | TTimestamp
  | TAddress
  | TOperation
  | TPackedStructure of structure_type
  | TContractInstance of structure_type
  | TEntryPoint of t
  | TView of t * t
  | TArrow of t * t
  | TVar of type_var
  | TForall of type_var * t

(** Type expressions used to define closures. *)
type type_exp = {
  lettypes : (string Love_pervasives.Ident.t * typedef) list;
  body : t;
}

type user_comp = type_name -> bool

(** Remove initial foralls of a type *)
val remove_foralls : t -> t

(** An empty contract signature *)
val unit_contract_sig : structure_sig

(** The type of an empty contract *)
val unit_contract_anon_type : structure_type

(** The default name of the empty contract *)
val unit_contract_name : string Love_pervasives.Ident.t

(** The named version of the empty contract *)
val unit_contract_named_type : structure_type

val find_content : string -> structure_sig -> sig_content option

val find_storage : structure_sig -> sig_type option

(** Defines an arrow type. The operator is right associative
    t1 @=> t2 @=> t3  =  TArrow (t1, TArrow (t2, t3))
*)
val ( @=> ) : t -> t -> t

(** Transforms a tvar t to TVar t *)
val tvar_to_ty : type_var -> t

val is_module : structure_sig -> bool

(** An entry point can be seen as a specific value of type TEntryPoint
    and as an application. Given the parameter, returns the lambda
    lambda type associated to an entry point application. *)
val entryPointType : ?type_storage:t -> t -> t

(** Same, but for view applications. *)
val viewType : ?type_storage:t -> t -> t -> t

(** Returns a fresh type variable. *)
val fresh_typevar : ?name:string -> ?tv_traits:traits -> unit -> type_var

(** Printers. Most of them have 2 modes: a debug mode (printing more informations)
    and a normal mode (more concise) *)
val pretty_typename :
  Format.formatter -> type_name -> unit

val pretty : Format.formatter -> t -> unit

val pp_typvar : Format.formatter -> type_var -> unit

val pp_constr : Format.formatter -> string * t list -> unit

val pp_record : Format.formatter -> string * t -> unit

val pp_trait : Format.formatter -> traits -> unit

val pp_contract_sig : Format.formatter -> structure_sig -> unit

val pp_ccontract_sig : Format.formatter -> structure_type -> unit

val pp_scons : Format.formatter -> string * t list -> unit

val pp_rfields : Format.formatter -> string * t -> unit

val pp_sumtyp :
  Format.formatter ->
  string -> string -> type_var list -> (string * t list) list -> recursive -> unit

val pp_rectyp :
  Format.formatter ->
  string -> string -> type_var list -> (string * t) list -> recursive -> unit

val pp_aliastyp :
  Format.formatter -> string -> string -> type_var list -> t -> unit

val pp_rec : Format.formatter -> recursive -> unit

val pp_typdef : Format.formatter -> name:string -> privacy:string -> typedef -> unit

val pp_sig_type :Format.formatter -> name:string -> sig_type -> unit

(** Deconstructs a TEntryPoint *)
val paramsFromEntryPoint : t -> t

(** Deconstructs a TView *)
val paramsFromView : t -> t * t

(** Returns the TUser corresponding to a type definition without applying any parameter. *)
val type_of_typedef : string -> typedef -> t

(** Returns the TUser corresponding to a type definition applying its parameters. *)
val instanciate_tdef : user_comp -> string -> typedef -> t list -> t

(** Returns the parameters of a type given its definition. *)
val typedef_parameters : typedef -> type_var list


val typ_by_str_args : string Ident.t -> t list -> t

(** Checks if a given type is comparable. *)
val isComparable : user_comp -> t -> bool

(** The different results from the equality test. *)
type equal_result =
    Aliases of type_var StringMap.t
  (** Success, given the map of aliases. The others are errors. *)

  | TypeIncompatibility of t * t
  (** Types are different *)

  | SigContentIncompatibility of sig_content * sig_content
  (** Sig Contents are different *)

  | ModuleVSContract
  (** Comparing a module and a contract *)

(** Returns None if types are equal, otherwise returns Some (t1, t2) where
    t1 and t2 are one of the differences between the types in argument.

    For example, equal (int * bool) (dun * bool) = Some (int, dun)
 *)
val equal : t -> t -> equal_result

(** Same than before, but returns a boolean. *)
val bool_equal : t -> t -> bool

(** Takes a lambda type in argument, returns 'Some s' if the type in argument
    is an entry point type with 's' the corresponding storage type. Returns None
    otherwise. *)
val isEntryPointType : t -> t option

(** Collections for type variables *)
module TypeVarSet : S.SET with type elt = type_var
module TypeVarMap : S.MAP with type key = type_var

(** Returns the set of free variables in a given type.
    NB: quantified variables (forall 'a. ...) are not free. *)
val fvars : t -> TypeVarSet.t

(** Returns the set of free variables in a given type. *)
val isListOf : t -> t option

(** Checks that a type variable occurs in a given type. *)
val occur : string -> t -> bool

(** Utils replacing type variables by actual types. *)
val replace_struct_sig : user_comp -> t TypeVarMap.t -> structure_sig -> structure_sig

val replace_tdef : user_comp -> t TypeVarMap.t -> typedef -> typedef

val replace_sig_type : user_comp -> t TypeVarMap.t -> sig_type -> sig_type

val replace_map : user_comp -> t TypeVarMap.t -> t -> t

val replace : user_comp -> TypeVarMap.key -> t -> t -> t

(** The result of subtyping *)
type subtyp_result =
    Ok
  (** Success. The rest are subtyping failures *)

  | TypeError of t * t (** Types are not subtypes *)
  | TypeDefError of (string * typedef) * (string * typedef) (** Type definitions are incompatible *)
  | SigContentError of (string * sig_content) * (string * sig_content)
  (** Contents are incompatible *)

  | Other of string (** Other errors *)

val str_subtyp_result : subtyp_result -> string

(** Tests that a type is strictly less general than another.
    This is mostly equality, except substructures are accepted.

    It takes two additional arguments:
    * aliases -> given a type name and a list of parameters, returns the
    corresponding type and its full definition path

    * signatures -> given a signature name, returns the corresponding signature

    It returns Some (t1, t2) if the types in argument are not subtypes with t1
    and t2 conflicting, otherwise returns None.
 *)
val subtyp :
  (string Love_pervasives.Ident.t -> structure_sig) ->
  t -> t -> subtyp_result

val sub_contract :
  (string Love_pervasives.Ident.t -> structure_sig) ->
  structure_sig -> structure_sig -> subtyp_result

val subtyp_typedef :
  (string Love_pervasives.Ident.t -> structure_sig) ->
  string -> typedef -> string -> typedef -> subtyp_result

(** Returns the list of type variables used in a type. *)
val typ_arguments : t -> type_var list

(** Returns the return type of an arrow or a TForall. If
    the argument is not such a type, returns the argument.
 *)
val return_type : t -> t

(** The default type for the polymorphic constructor None. *)
val type_cnone : t

(** The var type used in type_cnone *)
val tvo : type_var

(** The default type for the polymorphic empty list. *)
val type_empty_list : t

(** The var type used in type_empty_list *)
val tvl : type_var

(** The default type for the polymorphic empty set. *)
val type_empty_set : t

(** The var type used in type_empty_set *)
val tvset : type_var

(** The default type for the polymorphic empty map. *)
val type_empty_map : t

(** The var type used for keys in type_empty_map *)
val tvmkey : type_var

(** The var type used for bindings in type_empty_map *)
val tvmbnd : type_var

(** The default type for the polymorphic empty big map. *)
val type_empty_bigmap : t

(** The var type used for keys in type_empty_bigmap *)
val tvbmkey : type_var

(** The var type used for bindings in type_empty_bigmap *)
val tvbmbnd : type_var

(** Returning the corresponding types in t2 that are polymorphic in t1.
    The argument aliases corresponds to the type aliases (such as type t = int) *)
val search_aliases : user_comp -> (type_name -> typedef) -> t -> t -> t TypeVarMap.t

(** Counts the number of arguments of a value given its type. *)
val type_arity : t -> int

(** Compares types *)
val compare : t -> t -> int

(** Compares typedefs *)
val compare_typedef : typedef -> typedef -> int

(** Compares typevars *)
val compare_tvar : type_var -> type_var -> int

(** Comapres structure types *)
val compare_struct_type : structure_type -> structure_type -> int

(**  Compares signatures *)
val compare_sig_kind : structure_sig -> structure_sig -> int

(** Compares structure kinds *)
val compare_struct_kind : struct_kind -> struct_kind -> int
