(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Utils
open Love_runtime_ast

type vars = {
  tyv: string list;         (* binding / free *)
  var: string Ident.t list; (* binding / free *)
  str: string Ident.t list; (* binding / free *)
  sgn: string Ident.t list; (* no binding *)
  exn: string Ident.t list; (* no binding *)
  tyn: string Ident.t list; (* no binding *)
}

let empty_vars = { tyv = []; var = []; str = []; sgn = []; exn = []; tyn = [] }

let add_free_var (id : string Ident.t) all_bvs bv fv =
  if List.mem id bv then fv else
    match Ident.split id with
    | _, None -> add_uniq id fv
    | n, Some _ ->
        if List.mem (Ident.create_id n) all_bvs.str then fv else
          add_uniq id fv

let add_free_var_str (id : string) bv fv =
  if List.mem id bv then fv else add_uniq id fv

let add_binding id bv =
  add_uniq id bv

let pattern (bv : vars) (fv : vars) pattern =
  let rec aux (bv, fv) = function
    | RPAny -> bv, fv
    | RPVar v ->
        { bv with var = add_binding (Ident.create_id v) bv.var }, fv
    | RPAlias (p, v) ->
        let bv, fv = aux (bv, fv) p in
        { bv with var = add_binding (Ident.create_id v) bv.var }, fv
    | RPConst _c -> bv, fv
    | RPNone -> bv, fv
    | RPSome p -> aux (bv, fv) p
    | RPList pl -> List.fold_left aux (bv, fv) pl
    | RPTuple pl -> List.fold_left aux (bv, fv) pl
    | RPConstr (_pcstr, pl) -> List.fold_left aux (bv, fv) pl
    | RPContract (n, st) ->
        let fv = match st with
          | Named id -> { fv with sgn = add_free_var id bv bv.sgn fv.sgn }
          | Anonymous _ -> fv
        in
        let bv = { bv with str = add_binding (Ident.create_id n) bv.str } in
        bv, fv
  in
  aux (bv, fv) pattern

let rec typ (bv : vars) (fv : vars) =
  let open Love_type in
  function
  | TUnit | TBool | TString | TBytes | TInt | TNat | TDun | TKey
  | TKeyHash | TSignature | TTimestamp | TAddress | TOperation
  | TPackedStructure (Anonymous _) | TContractInstance (Anonymous _) ->
      fv
  | TOption t | TList t | TSet t | TEntryPoint t ->
      typ bv fv t
  | TMap (t1, t2) | TBigMap (t1, t2) | TArrow (t1, t2) | TView (t1, t2) ->
      typ bv (typ bv fv t1) t2
  | TTuple tl ->
      List.fold_left (typ bv) fv tl
  | TUser (tn, tl) ->
      let fv = List.fold_left (typ bv) fv tl in
      { fv with tyn = add_free_var tn bv bv.tyn fv.tyn }
  | TVar tv ->
      { fv with tyv = add_free_var_str tv.tv_name bv.tyv fv.tyv }
  | TForall (tv, t) ->
      typ { bv with tyv = add_binding tv.tv_name bv.tyv } fv t
  | TPackedStructure (Named id) | TContractInstance (Named id) ->
      { fv with sgn = add_free_var id bv bv.sgn fv.sgn }

let exp exp =
  let rec aux (bv : vars) (fv : vars) exp =
    match exp with
    | RConst (RCPrimitive (PBUnpack t)) ->
        typ bv fv t
    | RConst (RCPrimitive (PBMEmpty (t1, t2))) ->
        typ bv (typ bv fv t1) t2
    | RConst (RCPrimitive (PCSelf (StructType (Named id))
                          | PCAt (StructType (Named id)))) ->
       { fv with sgn = add_free_var id bv bv.sgn fv.sgn }
    | RConst (RCPrimitive (PCSelf (ContractInstance id)
                          | PCAt (ContractInstance id))) ->
       { fv with str = add_free_var id bv bv.str fv.str }
    | RConst _ ->
       fv
    | RVar id ->
       { fv with var = add_free_var id bv bv.var fv.var }
    | RLet { bnd_pattern; bnd_val; body } ->
       let fv = aux bv fv bnd_val in
       let bv, fv = pattern bv fv bnd_pattern in
       aux bv fv body
    | RLetRec { bnd_var; bnd_val; val_typ; body } ->
       let bv = { bv with var = add_binding (Ident.create_id bnd_var) bv.var} in
       let fv = typ bv fv val_typ in
       let fv = aux bv fv bnd_val in
       aux bv fv body
    | RLambda { args; body } ->
       let bv, fv = List.fold_left (fun (bv, fv) -> function
          | RPattern (p, t) ->
            let bv, fv = pattern bv fv p in
            bv, typ bv fv t
          | RTypeVar tv ->
            { bv with tyv = add_binding tv.tv_name bv.tyv }, fv
        ) (bv, fv) args
       in
       aux bv fv body
    | RApply { exp; args } ->
       List.fold_left (fun fv -> function
          | RExp e -> aux bv fv e
          | RType t -> typ bv fv t
        ) (aux bv fv exp) args
    | RSeq el ->
       List.fold_left (aux bv) fv el
    | RIf { cond; ifthen; ifelse } ->
       aux bv (aux bv (aux bv fv cond) ifthen) ifelse
    | RMatch { arg; cases } ->
       List.fold_left (fun fv (p, e) ->
          let bv, fv = pattern bv fv p in
          aux bv fv e
        ) (aux bv fv arg) cases
    | RConstructor { type_name = tn; constr = _; args; ctyps } ->
       let fv = { fv with tyn = add_free_var tn bv bv.tyn fv.tyn } in
       let fv = List.fold_left (typ bv) fv ctyps in
       List.fold_left (aux bv) fv args
    | RNone ->
       fv
    | RSome e ->
       aux bv fv e
    | RNil ->
       fv
    | RList (el, e) ->
       List.fold_left (aux bv) (aux bv fv e) el
    | RTuple el ->
       List.fold_left (aux bv) fv el
    | RProject { tuple; indexes = _ } ->
       aux bv fv tuple
    | RUpdate { tuple; updates } ->
       List.fold_left (fun fv (_, e) -> aux bv fv e) (aux bv fv tuple) updates
    | RRecord { type_name = tn; contents = rec_def } ->
       let fv = { fv with tyn = add_free_var tn bv bv.tyn fv.tyn } in
       List.fold_left (fun fv (_, e) -> aux bv fv e) fv rec_def
    | RGetField { record; field = _ } ->
       aux bv fv record
    | RSetFields { record; updates } ->
       List.fold_left (fun fv (_, e) -> aux bv fv e) (aux bv fv record) updates
    | RPackStruct (RNamed id) ->
       { fv with str = add_free_var id bv bv.str fv.str }
    | RPackStruct (RAnonymous _) ->
       fv (* Anonymous structures are already closed *)
    | RRaise { exn; args; loc = _ } ->
       let fv = match exn with
         | RFail t -> typ bv fv t
         | RException id -> { fv with exn = add_free_var id bv bv.exn fv.exn }
       in
       List.fold_left (aux bv) fv args
    | RTryWith { arg; cases } ->
       List.fold_left (fun fv ((exn, pl), e) ->
          let exn, fv = match exn with
            | RFail t -> "", typ bv fv t
            | RException id ->
                Ident.get_final id,
                { fv with exn = add_free_var id bv bv.exn fv.exn }
          in
          let bv, fv = pattern bv fv (RPConstr (exn, pl)) in
          aux bv fv e
        ) (aux bv fv arg) cases
  in
  aux empty_vars empty_vars exp
