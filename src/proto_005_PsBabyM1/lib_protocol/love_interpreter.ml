(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Error_monad
open Love_pervasives
open Exceptions
open Love_context
open Love_runtime_ast
open Love_value
open Value

let () = Love_prim_list.init ()

let consume_gas ctxt gas =
  Lwt.return (Alpha_context.Gas.consume ctxt.actxt gas
              >|? fun actxt -> { ctxt with actxt })

let deconstruct_pattern pattern value =
  let exception PatternDoesNotMatch in
  let add_uniq x v pxl =
    let id = Ident.create_id x in
    if not (List.mem_assoc id pxl) then (id, Value.Local v) :: pxl
    else raise (InvariantBroken "Variable bound several times in pattern")
  in
  let add_uniq_struct x (_st, a) pxl =
    let id = Ident.create_id x in
    let path = Ident.create_id (Contract_repr.to_b58check a) in
    if not (List.mem_assoc id pxl) then (id, Value.Pointer path) :: pxl
    else raise (InvariantBroken "Variable bound several times in pattern")
  in
  let rec aux pxl p v = match p, v with
    | RPAny, _ -> pxl
    | RPVar x, _ -> { pxl with values = add_uniq x v pxl.values }
    | RPAlias (p, x), _ ->
        let pxl = aux pxl p v in
        { pxl with values = add_uniq x v pxl.values }
    | RPConst c, v when Value.(equal (of_const c) v) -> pxl
    | RPNone, VNone -> pxl
    | RPSome p, VSome v -> aux pxl p v
    | RPList pl, VList vl -> aux_list pxl pl vl
    | RPTuple pl, VTuple vl ->
       begin
         try List.fold_left2 aux pxl pl vl
         with Invalid_argument _ ->
           raise (InvariantBroken "Wrong arity in pattern")
       end
    | RPConstr (pcstr, pl), VConstr (vcstr, [VTuple vl])
    | RPConstr (pcstr, pl), VConstr (vcstr, vl)
         when String.equal pcstr vcstr ->
       begin
         try List.fold_left2 aux pxl pl vl
         with Invalid_argument _ ->
           raise (InvariantBroken "Wrong arity in pattern")
       end
    | RPContract (n, _st), VContractInstance (st, a) ->
        { pxl with structs = add_uniq_struct n (st, a) pxl.structs }
    | (RPConst _ | RPNone | RPSome _ | RPList _
      | RPTuple _ | RPConstr _ | RPContract _), _ ->
       raise PatternDoesNotMatch (* or matching stuff of different types *)
  and aux_list pxl pl vl = match pl, vl with
    | [], [] -> pxl
    | p :: [], vl -> aux pxl p (VList vl)
    | p :: pl, v :: vl -> aux_list (aux pxl p v) pl vl
    | _ :: _, [] -> raise PatternDoesNotMatch
    | [], _ :: _ -> raise PatternDoesNotMatch (* should not occur *)
  in
  try
(* only values and structs *)
    let pxl = aux { values = []; structs = []; sigs = []; exns = [];
                    types = []; tvars = [] } pattern value in
    Some { values = List.rev pxl.values; structs = List.rev pxl.structs;
           sigs = List.rev pxl.sigs; exns = List.rev pxl.exns;
           types = List.rev pxl.types; tvars = List.rev pxl.tvars }
  with PatternDoesNotMatch -> None

let rec eval :
  Love_context.t -> Love_env.t -> Love_runtime_ast.exp -> 'a tzresult Lwt.t =
  fun ctxt env exp ->

  let module G = Love_gas.Cost_of in
  let env = Love_env.inc_depth env in
  consume_gas ctxt G.cycle >>=? fun ctxt ->
  match exp with

  | RConst c ->
     consume_gas ctxt G.const >>|? fun ctxt ->
     (ctxt, Value.of_const c)

  | RVar v ->
     consume_gas ctxt G.var >>=? fun ctxt ->
     Love_env.find_value_opt ctxt env v >>|? fun (ctxt, res) ->
     begin match res with
     | Some v -> ctxt, v
     | None -> raise (InvariantBroken (
                          Format.asprintf "Variable not found: %a"
                            Ident.print_strident v))
     end

  | RLet { bnd_pattern; bnd_val; body } ->
     consume_gas ctxt G.letin >>=? fun ctxt ->
     eval ctxt env bnd_val >>=? fun (ctxt, bval) ->
     begin match deconstruct_pattern bnd_pattern bval with
       | Some bl -> eval ctxt (Love_env.add_bindings env bl) body
       | None -> raise (InvariantBroken "Incomplete pattern matching in let")
     end

  | RLetRec { bnd_var = v; bnd_val; body; _ } ->
     consume_gas ctxt G.letrec >>=? fun ctxt ->
     let id = Ident.create_id v in
     let env = Love_env.add_local_value env id VUnit in (*Dummy, could use ptr*)
     eval ctxt env bnd_val >>=? fun (ctxt, bval) ->
     let id, v = match bval with
       | VClosure { call_env; lambda } ->
          let values = List.filter (fun (id', _) ->
              not (Ident.equal String.equal id id')) call_env.values in
          let rec new_call_env =
            { call_env with
              values =
                (id, (Local (VClosure { call_env = new_call_env;
                                        lambda }))) :: values } in
          List.hd new_call_env.values (* New binding is at head *)
       | _ ->
          raise (InvariantBroken "`let rec` must bind a function")
     in
     eval ctxt (Love_env.add_value env id v) body

  | RLambda lambda -> (* G.tlambda ? *)
     consume_gas ctxt G.lambda >>=? fun ctxt ->
     let free_vars = Love_free_vars.exp exp in
     Love_env.get_closure ctxt env free_vars >>|? fun (ctxt, call_env) ->
     (ctxt, VClosure { call_env; lambda })

  | RApply { exp; args } -> (* G.tapply *)
     consume_gas ctxt G.apply >>=? fun ctxt ->
     eval ctxt env exp >>=? fun (ctxt, v) ->
     apply_value ctxt env v (Exp args) >>|? fun (ctxt, v) ->
     (ctxt, v)

  | RSeq el ->
     consume_gas ctxt G.seq >>=? fun ctxt ->
     fold_left_s (fun (ctxt, _v) e -> eval ctxt env e) (ctxt, VUnit) el

  | RIf { cond; ifthen; ifelse } ->
     consume_gas ctxt G.ite >>=? fun ctxt ->
     eval ctxt env cond >>=? fun (ctxt, res) ->
     begin match res with
       | VBool true -> eval ctxt env ifthen
       | VBool false -> eval ctxt env ifelse
       | _ -> raise (InvariantBroken "If condition must be a boolean")
     end

  | RMatch { arg; cases } ->
     let rec eval_match ctxt env c = function
       | [] -> raise (InvariantBroken "Incomplete pattern matching in match")
       | (p, e) :: cases ->
          match deconstruct_pattern p c with
          | Some bl -> eval ctxt (Love_env.add_bindings env bl) e
          | None -> eval_match ctxt env c cases
     in
     consume_gas ctxt G.matchwith >>=? fun ctxt ->
     eval ctxt env arg >>=? fun (ctxt, c) ->
     eval_match ctxt env c cases

  | RConstructor { type_name = _; constr; args; ctyps = _ } ->
     consume_gas ctxt (G.construct (List.length args)) >>=? fun ctxt ->
     fold_left_s (fun (ctxt, vl) e ->
         eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, v :: vl)
       ) (ctxt, []) args >>|? fun (ctxt, vl) ->
     (ctxt, VConstr (constr, List.rev vl))

  | RNone ->
     consume_gas ctxt G.opt >>|? fun ctxt ->
     (ctxt, VNone)

  | RSome e ->
     consume_gas ctxt G.opt >>=? fun ctxt ->
     eval ctxt env e >>|? fun (ctxt, v) ->
     (ctxt, VSome v)

  | RNil ->
     consume_gas ctxt (G.list 1) >>|? fun ctxt ->
     (ctxt, VList [])

  | RList (el, e) ->
     begin
       consume_gas ctxt (G.list (List.length el)) >>=? fun ctxt ->
       eval ctxt env e >>=? function
       | ctxt, VList vl ->
          fold_left_s (fun (ctxt, vl) e ->
              eval ctxt env e >>|? fun (ctxt, v) ->
              (ctxt, v :: vl)
            ) (ctxt, vl) (List.rev el) >>|? fun (ctxt, vl) ->
          (ctxt, VList vl)
       | _ -> raise (InvariantBroken
                       "Applying const to something other than a list")
     end

  | RTuple el ->
     consume_gas ctxt (G.tuple (List.length el)) >>=? fun ctxt ->
     fold_left_s (fun (ctxt, vl) e ->
         eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, v :: vl)
       ) (ctxt, []) el >>|? fun (ctxt, vl) ->
     (ctxt, VTuple (List.rev vl))

  | RProject { tuple; indexes } ->
     consume_gas ctxt G.project >>=? fun ctxt ->
     eval ctxt env tuple >>|? fun (ctxt, res) ->
     begin match res with
     | VTuple vl ->
       let vl = List.map (fun i ->
           match List.nth_opt vl i with
           | Some v -> v
           | None -> raise (InvariantBroken "Project outside of tuple")
         ) indexes
       in
       begin match vl with
         | [v] -> ctxt, v
         | vl -> ctxt, VTuple vl
       end
     | _ -> raise (InvariantBroken "Project must be applied to tuple")
     end

  | RUpdate { tuple; updates } ->
     consume_gas ctxt (G.update (List.length updates)) >>=? fun ctxt ->
     eval ctxt env tuple >>=? fun (ctxt, res) ->
     begin match res with
     | VTuple vl ->
        fold_left_s (fun (ctxt, ul) (i, e) ->
            eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, (i, v) :: ul)
          ) (ctxt, []) updates >>|? fun (ctxt, ul) ->
        let vl, _ =
          List.fold_left (fun (vl, idx) v ->
              match List.assoc_opt idx ul with
              | Some uv -> (uv :: vl, idx + 1)
              | None -> (v :: vl, idx + 1)
            ) ([], 0) vl
        in
        ctxt, VTuple (List.rev vl)
     | _ -> raise (InvariantBroken "Update must be applied to tuple")
     end

  | RRecord { type_name = _; contents = rec_def } ->
     consume_gas ctxt (G.record (List.length rec_def)) >>=? fun ctxt ->
     fold_left_s (fun (ctxt, l) (f, e) ->
         eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, (f, v) :: l)
       ) (ctxt, []) rec_def >>|? fun (ctxt, l) ->
     (ctxt, VRecord (List.rev l))

  | RGetField { record; field } ->
     consume_gas ctxt G.getfield >>=? fun ctxt ->
     eval ctxt env record >>|? fun (ctxt, res) ->
     begin match res with
     | VRecord fl ->
        begin match List.assoc_opt field fl with
        | Some v -> ctxt, v
        | None -> raise (InvariantBroken "GetField of inexistant field")
        end
     | _ -> raise (InvariantBroken "GetField must be applied to record")
     end

  | RSetFields { record; updates } ->
     consume_gas ctxt (G.setfields (List.length updates))>>=? fun ctxt ->
     eval ctxt env record >>=? fun (ctxt, res) ->
     begin match res with
     | VRecord fl ->
        fold_left_s (fun (ctxt, ul) (f, e) ->
            eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, (f, v) :: ul)
          ) (ctxt, []) updates >>|? fun (ctxt, ul) ->
        let fl =
          List.fold_left (fun fl (f, v) ->
              match List.assoc_opt f ul with
              | Some uv -> (f, uv) :: fl
              | None -> (f, v) :: fl
            ) [] fl
        in
        ctxt, VRecord (List.rev fl)
     | _ -> raise (InvariantBroken "GetField must be applied to record")
     end

  | RPackStruct sr ->
     consume_gas ctxt G.pack_contract >>=? fun ctxt ->
     begin match sr with
     | RAnonymous s ->
        if Love_runtime_ast.is_module s then
          raise (InvariantBroken "Only contracts can be made first-class");
        eval_structure ctxt [Constants.anonymous]
          (Love_env.re_init env Constants.anonymous) s
        >>|? fun (ctxt, _env, s) ->
        ctxt, VPackedStructure
          (Constants.anonymous_id,
           { version = Options.version; root_struct = s })
     | RNamed sn ->
        Love_env.find_struct_opt ctxt env sn >>|? fun (ctxt, res) ->
        begin match res with
        | Some (path, s) ->
           if Love_value.LiveStructure.is_module s then
             raise (InvariantBroken "Only contracts can be made first-class");
           ctxt, VPackedStructure
             (path, { version = Options.version; root_struct = s })
        | None -> raise (InvariantBroken
                           (Format.asprintf "Structure not found: %a"
                              Ident.print_strident sn))
        end
     end

  | RRaise { exn; args; loc } ->
    consume_gas ctxt (G.raiseexn (List.length args)) >>=? fun ctxt ->
    fold_left_s (fun (ctxt, vl) e ->
        eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, v :: vl)
      ) (ctxt, []) args >>|? fun (ctxt, vl) ->
    raise (UserException (ctxt, (exn, List.rev vl), loc))

  | RTryWith { arg; cases } ->
    consume_gas ctxt G.trywith >>=? fun ctxt ->
    Lwt.catch (fun () -> eval ctxt env arg)
      (function
        | (UserException (ctxt, (exn, vl), _)) as e ->
           let rec eval_match ctxt env exn vl = function
             | [] -> raise e (* Uncaught : re-raise *)
             | ((exn', pl), e) :: cases ->
               if Love_runtime_ast.exn_equal exn exn'
               then match deconstruct_pattern
                            (RPConstr (exn_id exn', pl))
                            (VConstr ((exn_id exn), vl)) with
               | Some bl -> eval ctxt (Love_env.add_bindings env bl) e
               | None -> eval_match ctxt env exn vl cases
               else eval_match ctxt env exn vl cases
           in eval_match ctxt env exn vl cases
        | e -> raise e (* Not a user exception : re-raise *)
       )

(* Avoid reconstructing closures on each application,
   try applying as many arguments as possible at once *)
and apply_exp ctxt env exp al (call_env: Value.env) :
    (Love_context.t * Love_value.Value.t) Error_monad.tzresult Lwt.t =
  match exp, al with
  | RLambda { args = (RTypeVar tv) :: args; body }, Exp ((RType t) :: al) ->
     let body = match args with [] -> body | _ -> RLambda { args; body } in
     let call_env = { call_env with
                      tvars = (tv.tv_name, t) :: call_env.tvars } in
     apply_exp ctxt env body (Exp al) call_env
  | RLambda { args = (RPattern (arg_pattern, _)) :: args; body },
     Exp ((RExp e) :: al) ->
     eval ctxt env e >>=? fun (ctxt, v) ->
     begin match deconstruct_pattern arg_pattern v with
      | Some bl ->
         let body = match args with [] -> body | _ -> RLambda { args; body } in
         let call_env = { values = bl.values @ call_env.values;
                          structs = bl.structs @ call_env.structs;
                          sigs = bl.sigs @ call_env.sigs;
                          exns = bl.exns @ call_env.exns;
                          types = bl.types @ call_env.types;
                          tvars = bl.tvars @ call_env.tvars } in
         apply_exp ctxt env body (Exp al) call_env
      | None -> raise (InvariantBroken "Incomplete pattern matching in lambda")
     end
  | RLambda { args = (RPattern (arg_pattern, _)) :: args; body },
     Val (v :: al) ->
     begin match deconstruct_pattern arg_pattern v with
      | Some bl ->
         let body = match args with [] -> body | _ -> RLambda { args; body } in
         let call_env = { values = bl.values @ call_env.values;
                          structs = bl.structs @ call_env.structs;
                          sigs = bl.sigs @ call_env.sigs;
                          exns = bl.exns @ call_env.exns;
                          types = bl.types @ call_env.types;
                          tvars = bl.tvars @ call_env.tvars } in
         apply_exp ctxt env body (Val al) call_env
      | None -> raise (InvariantBroken "Incomplete pattern matching in lambda")
     end
  | RLambda lambda, (Exp [] | Val []) ->
     return (ctxt, VClosure { call_env; lambda })
  | _, _ ->
     (* could use a clean env *)
     eval ctxt (Love_env.add_bindings env call_env) exp >>=? fun (ctxt, v) ->
     apply_value ctxt env v al

and apply_value ctxt env value args :
    (Love_context.t * Love_value.Value.t) Error_monad.tzresult Lwt.t =
  let module Prim = Love_prim_interp in
  match value, args with
  | VPrimitive (prim, al), Exp (RExp a :: args) ->
     eval ctxt env a >>=? fun (ctxt, v) ->
     Prim.eval apply_value ctxt env prim (v :: al) >>=? fun (ctxt, v) ->
     apply_value ctxt env v (Exp args)
  | VPrimitive (prim, al), Val (v :: args) ->
     Prim.eval apply_value ctxt env prim (v :: al) >>=? fun (ctxt, v) ->
     apply_value ctxt env v (Val args)
  | VClosure { call_env; lambda }, _ ->
     apply_exp ctxt env (RLambda lambda) args call_env
  | _, Exp (RType _ :: args) -> (* ignore type applications on primitives *)
     apply_value ctxt env value (Exp args) (* and values other than closures *)
  | _, (Exp [] | Val []) -> return (ctxt, value)
  | _v, _ -> raise (InvariantBroken "Can only apply functions/primitives")

and eval_structure ctxt path env
    ({ structure_content; kind } : Love_runtime_ast.structure) :
    (Love_context.t * Love_env.t * LiveStructure.t) Error_monad.tzresult Lwt.t =
  let open LiveStructure in
  fold_left_s (fun (ctxt, env, content) (n, c) ->
      let id = Ident.create_id n in
      match c with
      | RDefType (k, td) ->
         let path = Ident.put_in_namespaces path id in
         let env = Love_env.add_type_name env id path in
         let content = (n, VType (k, td)) :: content in
         return (ctxt, env, content)
      | RDefException tl ->
         let path = Ident.put_in_namespaces path id in
         let env = Love_env.add_exn_name env id path in
         let content = (n, VException tl) :: content in
         return (ctxt, env, content)
      | REntry { entry_code; entry_fee_code; entry_typ } ->
(* TODO : for root struct, remove everything from closure and add it to env on load (this makes smaller closures) *)
         eval ctxt env entry_code >>=? fun (ctxt, ec) ->
         begin match ec, entry_fee_code with
           | VClosure { lambda = l; _ }, Some fc ->
             begin match Love_ast_utils.lambdas_to_params (RLambda l) with
               | pt1 :: pt2 :: pt3 :: _ ->
                 let lam =
                   Love_ast_utils.params_to_lambdas [pt1; pt2; pt3] fc in
                 eval ctxt env lam >>|? fun (ctxt, fee_code) ->
                 (ctxt, Some fee_code)
               | _ -> raise (InvariantBroken "Invalid entry point")
             end
           | _ -> return (ctxt, None)
         end >>|? fun (ctxt, fc) ->
         let path = Ident.put_in_namespaces path id in
         let env = Love_env.add_inlined_value env id path ec in
         let entry = { ventry_code = ec; ventry_fee_code = fc;
                       ventry_typ = entry_typ } in
         let content = (n, VEntry entry) :: content in
         ctxt, env, content
      | RView { view_code; view_typ } ->
         eval ctxt env view_code >>|? fun (ctxt, vc) ->
         let path = Ident.put_in_namespaces path id in
         let env = Love_env.add_inlined_value env id path vc in
         let view = { vview_code = vc; vview_typ = view_typ } in
         let content = (n, VView view) :: content in
         ctxt, env, content
      | RValue { value_code; value_typ; value_visibility } ->
         eval ctxt env value_code >>|? fun (ctxt, v) ->
         let path = Ident.put_in_namespaces path id in
         let env = Love_env.add_inlined_value env id path v in
         let value = { vvalue_code = v; vvalue_typ = value_typ;
                       vvalue_visibility = value_visibility } in
         let content = (n, VValue value) :: content in
         ctxt, env, content
      | RStructure s ->
         eval_structure ctxt (n :: path) env s >>|? fun (ctxt, _env, s) ->
         let path = Ident.put_in_namespaces path id in
         let env = Love_env.add_inlined_struct env id path s in
         let content = (n, VStructure s) :: content in
         ctxt, env, content
      | RSignature s ->
         let path = Ident.put_in_namespaces path id in
         let env = Love_env.add_inlined_sig env id path s in
         let content = (n, VSignature s) :: content in
         return (ctxt, env, content)
    ) (ctxt, env, []) structure_content >>|? fun (ctxt, env, content) ->
  ctxt, env, { content = List.rev content; kind }

(** Normalize a script, i.e extract its fee code and return it separately *)
let normalize_script actxt (code : LiveContract.t) :
    (Alpha_context.t * LiveContract.t * FeeCode.t) Error_monad.tzresult Lwt.t =
  let contract, fee_code = Love_value.normalize_contract code in
  return (actxt, contract, fee_code)

(* add denormalize *)

(** Typecheck some code *)
let typecheck_code actxt (code : Love_ast.top_contract) :
    Alpha_context.t Error_monad.tzresult Lwt.t =
  Lwt.catch (fun () ->
      let Love_ast.{ version = _; code } = code in
      let type_env = Love_tenv.empty code.kind () () in
      let _code, _type_env =
        Love_typechecker.typecheck_struct None type_env code in
      return actxt
    ) Love_errors.exn_handler

(** Typecheck a value *)
let typecheck_data actxt (data : Value.t) (typ : Love_type.t) :
    Alpha_context.t Error_monad.tzresult Lwt.t =
  Lwt.catch (fun () ->
      let ctxt = Love_context.init actxt in
      let type_env = Love_tenv.empty Module () () in
      let expected_ty = Love_type.{ lettypes = []; body = typ } in
      Love_typechecker.typecheck_value type_env ctxt expected_ty data
      >>|? fun ctxt -> ctxt.actxt
    ) Love_errors.exn_handler

(** Typecheck the contract and turn it into a live_contract *)
let typecheck actxt ~(code: Love_ast.top_contract) ~(storage: Value.t) ~self :
  (Alpha_context.t * LiveContract.t * Value.t)
    Error_monad.tzresult Lwt.t =
  Lwt.catch (fun () ->
      let version = code.version in
      let ctxt = Love_context.init ~self actxt in
      let tenv = Love_tenv.empty code.code.kind () () in
      Love_typechecker.typecheck_top_contract_deps None ctxt tenv code
      >>=? fun (ctxt, (code, type_env)) ->
      let code = Love_translator.translate_structure type_env code.code in
      let kt1 = Contract_repr.to_b58check self in
      eval_structure ctxt [kt1] (Love_env.init kt1) code
      >>=? fun (ctxt, env, root_struct) ->
      begin
        match List.assoc_opt "__init_storage" root_struct.content with
        | Some (VValue { vvalue_code = VClosure _ as fct;
                         vvalue_typ = TArrow (pt, _); vvalue_visibility }) ->
            let root_struct = match vvalue_visibility with
              | Public -> root_struct
              | Private -> { root_struct with
                               content = List.filter (fun (n, _c) ->
                                   not (String.equal n "__init_storage")
                                 ) root_struct.content }
            in
            let expected_ty = Love_type.{ lettypes = []; body = pt } in
            Love_typechecker.typecheck_value type_env ctxt expected_ty storage
            >>=? fun ctxt ->
            apply_value ctxt (Love_env.init kt1) fct (Val [storage])
            >>|? fun (ctxt, storage) ->
            ctxt, root_struct, Love_translator.inline_value ctxt storage
        | Some _ ->
            raise (InvariantBroken "Bad storage initializer")
        | None ->
            let storage_type =
              Love_type.{ lettypes = [];
                          body = TUser (Ident.create_id "storage", []) } in
            Love_typechecker.typecheck_value type_env ctxt storage_type storage
            >>|? fun ctxt ->
            ctxt, root_struct, storage
      end
      >>=? fun (ctxt, root_struct, storage) ->
      Love_translator.make_types_absolute_in_value ctxt env storage
      >>|? fun (ctxt, storage_abs) ->
      let code = LiveContract.{ version; root_struct } in
      ctxt.actxt, code, storage_abs
    ) Love_errors.exn_handler

let execute actxt ~source ~payer ~self
    ~(code: LiveContract.t) ~(storage: Value.t)
    ~entrypoint ~(parameter: Value.t) ~amount :
  (Alpha_context.t *
   (Value.t * Op.internal_operation list *
    Op.big_map_diff option * Value.t))
    Error_monad.tzresult Lwt.t =
  let module PI = Love_prim_interp in
  Lwt.catch (fun () ->
      let ctxt =
        Love_context.init ~self ~source ~payer ~amount ~code ~storage actxt in
      match List.assoc_opt entrypoint code.root_struct.content with
      | Some (VEntry { ventry_code = VClosure _ as fct; ventry_typ; _ }) ->
        let deps = match code.root_struct.kind with
          | Love_type.Module -> []
          | Love_type.Contract l -> l in
        let tenv = Love_tenv.empty code.root_struct.kind () () in
        Love_typechecker.extract_deps None ctxt tenv deps
        >>=? fun (ctxt, deps_env) ->
        let root_sig =
          Love_value.sig_of_structure ~only_typedefs:true code.root_struct in
        let type_env =
          Love_tenv.contract_sig_to_env None () root_sig deps_env in
        let expected_ty = Love_type.{ lettypes = []; body = ventry_typ } in
        Love_typechecker.typecheck_value type_env ctxt expected_ty parameter
        >>=? fun ctxt ->
        PI.collect_big_maps ctxt parameter >>=? fun (to_duplicate, ctxt) ->
        PI.collect_big_maps ctxt storage >>=? fun (to_update, ctxt) ->
        let args = storage :: (VDun amount) :: parameter :: [] in
        apply_value ctxt (Love_env.init "") fct (Val args) >>=?
        begin function
          | ctxt, VTuple [ VList ops; storage ] ->
            let storage = Love_translator.inline_value ctxt storage in
            PI.extract_big_map_diff ctxt ~temporary:false
              ~to_duplicate ~to_update storage
            >>|? fun (storage, big_map_diff, ctxt) ->
            let ops, op_diffs = List.split (List.map (function
                | VOperation (op, diff) -> op, diff
                | _ -> failwith "Not an operation") ops)
            in
            let big_map_diff = match
                List.flatten (List.map (Option.unopt ~default:[])
                                (op_diffs @ [ big_map_diff ])) with
            | [] -> None
            | diff -> Some diff
            in
            ctxt.actxt, (VUnit, ops, big_map_diff, storage)
          | _ -> raise BadReturnType
        end
      | _ -> raise BadEntryPoint
    ) Love_errors.exn_handler

let execute_fee_script actxt ~source ~payer ~self
    ~(fee_code: FeeCode.t) ~(storage: Value.t)
    ~entrypoint ~(parameter: Value.t) ~amount :
  (Alpha_context.t * (Tez_repr.t * Z.t)) Error_monad.tzresult Lwt.t =
  Lwt.catch (fun () ->
      let ctxt =
        Love_context.init ~self ~source ~payer ~amount ~storage actxt in
      let FeeCode.{ version = _; root_struct; fee_codes } = fee_code in
      match List.assoc_opt entrypoint fee_codes with
      | Some (fee_code, ptype) ->
        begin
          let deps = match root_struct.kind with
            | Love_type.Module -> []
            | Love_type.Contract l -> l in
          let tenv = Love_tenv.empty root_struct.kind () () in
          Love_typechecker.extract_deps None ctxt tenv deps
          >>=? fun (ctxt, deps_env) ->
          let root_sig =
            Love_value.sig_of_structure ~only_typedefs:true root_struct in
          let type_env =
            Love_tenv.contract_sig_to_env None () root_sig deps_env in
          let expected_ty = Love_type.{ lettypes = []; body = ptype } in
          Love_typechecker.typecheck_value type_env ctxt expected_ty parameter
          >>=? fun ctxt ->
          let args = storage :: (VDun amount) :: parameter :: [] in
          apply_value ctxt (Love_env.init "") fee_code (Val args)
          >>|? function
            | ctxt, VTuple [ max_fee; max_storage ] ->
                let max_fee, max_storage = match max_fee, max_storage with
                  | VDun mf, VNat ms -> mf, ms
                  | _ -> failwith "Not a (dun, nat) pair"
                in
                ctxt.actxt, (max_fee, max_storage)
            | _ -> raise BadReturnType
        end
      | _ -> raise BadEntryPoint
    ) Love_errors.exn_handler

let storage_big_map_diff actxt storage :
  (Alpha_context.t * Value.t * Op.big_map_diff option)
    Error_monad.tzresult Lwt.t =
  let module PI = Love_prim_interp in
  PI.collect_big_maps (Love_context.init actxt) storage
  >>=? fun (to_duplicate, ctxt) ->
  PI.extract_big_map_diff ctxt ~temporary:false ~to_duplicate
    ~to_update:Collections.ZSet.empty storage
  >>|? fun (storage, big_map_diff, ctxt) ->
  ctxt.actxt, storage, big_map_diff
