(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Exceptions
open Collections

type primitive_kind =
  | PrimFunction
  | PrimInfix
  | PrimPrefix

type generic_primitive = {
  prim_name : string ;
  prim_kind : primitive_kind ;
  prim_type : Love_type.t ;
  prim_arity : int ;
  prim_id : int ;
}

type t =
  (* Comparisons *)
  | PCompare | PEq | PNe | PLt | PLe | PGt | PGe

  (* Arithmetic on Integers/Naturals/Duns/Timestamps *)
  | PIAdd  | PISub  | PIMul  | PIDiv  | PINeg | PIAbs
  | PNAdd  | PNSub  | PNMul  | PNDiv  | PNNeg | PNAbs
  | PNIAdd | PNISub | PNIMul | PNIDiv
  | PINAdd | PINSub | PINMul | PINDiv
  | PDAdd  | PDSub  | PDDiv
  | PDIMul | PIDMul | PDIDiv
  | PDNMul | PNDMul | PDNDiv
  | PTSub
  | PTIAdd | PITAdd | PTISub
  | PTNAdd | PNTAdd | PTNSub

  (* Boolean operators *)
  | PBAnd | PBOr | PBXor | PBNot

  (* Bitwise operators *)
  | PIAnd | PIOr | PIXor | PINot | PILsl | PILsr
  | PNAnd | PNOr | PNXor | PNLsl | PNLsr

  (* List primitives *)
  | PLLength | PLCons | PLRev | PLConcat
  | PLIter | PLMap | PLFold | PLMapFold

  (* Set primitives *)
  | PSEmpty | PSCard | PSAdd | PSRemove | PSMem
  | PSIter | PSMap | PSFold | PSMapFold

  (* Map primitives *)
  | PMEmpty | PMCard | PMAdd | PMRemove | PMMem | PMFind
  | PMIter | PMMap | PMFold | PMMapFold

  (* Map primitives *)
  | PBMEmpty of Love_type.t * Love_type.t
  | PBMAdd | PBMRemove | PBMMem | PBMFind

  (* Loop primitive *)
  | PLLoop

  (* String primitives *)
  | PSLength | PSConcat | PSSlice

  (* Bytes primitives *)
  | PBLength | PBConcat | PBSlice
  | PBPack | PBUnpack of Love_type.t
  | PBHash

  (* Cryptographic operations *)
  | PCBlake2b | PCSha256 | PCSha512
  | PCHashKey | PCCheck

  (* Contract interactions *)
  | PCAddress
  | PCSelf of Love_type.contract_type
  | PCAt of Love_type.contract_type
  | PCCall | PCView | PCCreate | PCSetDeleg

  (* Account interactions *)
  | PADefault | PATransfer | PABalanceOf
  | PAGetInfo | PAManage

  (* General information *)
  | PCBalance (* balance of current contract *)
  | PCTime (* timestamp of block in which the transaction is included *)
  | PCAmount (* amount of dun transferred by the current operation *)
  | PCGas (* amount of gas remaining to execute the rest of the transaction *)
  | PCrSelf (* address of current contract *)
  | PCSource (* address that initiated the current top-level transaction *)
  | PCSender (* address that initiated the current transaction *)
  | PCLevel (* current level *)
  | PCCycle (* current cycle *)

  (* Conversions *)
  | PAddressOfKeyHash
  | PKeyHashOfAddress

  | PGenericPrimitive of generic_primitive

let name = function
  | PCompare -> "compare"
  | PEq -> "=" | PNe -> "<>"
  | PLt -> "<" | PLe -> "<="
  | PGt -> ">" | PGe -> ">="
  | PIAdd -> "+" | PISub -> "-"
  | PIMul -> "*" | PIDiv -> "/"
  | PINeg -> "-" | PIAbs -> "iabs"
  | PNAdd -> "++" | PNSub -> "-+"
  | PNMul -> "*+" | PNDiv -> "/+"
  | PNNeg -> "-+" | PNAbs -> "abs"
  | PNIAdd -> "++!" | PINAdd -> "+!+"
  | PNISub -> "-+!" | PINSub -> "-!+"
  | PNIMul -> "*+!" | PINMul -> "*!+"
  | PNIDiv -> "/+!" | PINDiv -> "/!+"
  | PDAdd -> "+$" | PDSub -> "-$" | PDDiv -> "/$"
  | PDIMul -> "*$!" | PIDMul -> "*!$" | PDIDiv -> "/$!"
  | PDNMul -> "*$+" | PNDMul -> "*+$" | PDNDiv -> "/$+"
  | PTSub ->  "-:"
  | PTIAdd -> "+:!" | PITAdd -> "+!:" | PTISub -> "-:!"
  | PTNAdd -> "+:+" | PNTAdd -> "++:" | PTNSub -> "-:+"
  | PBAnd -> "&&" | PBOr -> "||"
  | PBXor -> "|&" | PBNot -> "not"
  | PIAnd -> "land" | PIOr -> "lor"
  | PIXor -> "lxor" | PINot -> "lnot"
  | PILsl -> "lsl" | PILsr -> "lsr"
  | PNAnd -> "nland" | PNOr -> "nlor"
  | PNXor -> "nlxor"
  | PNLsl -> "nlsl" | PNLsr -> "nlsr"
  | PLLength -> "List.length"
  | PLCons -> "List.cons"
  | PLRev -> "List.rev"
  | PLConcat -> "List.concat"
  | PLIter -> "List.iter"
  | PLMap -> "List.map"
  | PLFold -> "List.fold"
  | PLMapFold -> "List.map_fold"
  | PSCard -> "Set.cardinal"
  | PSEmpty -> "Set.empty"
  | PSAdd -> "Set.add"
  | PSRemove -> "Set.remove"
  | PSMem -> "Set.mem"
  | PSIter -> "Set.iter"
  | PSMap -> "Set.map"
  | PSFold -> "Set.fold"
  | PSMapFold -> "Set.map_fold"
  | PMCard -> "Map.cardinal"
  | PMEmpty -> "Map.empty"
  | PMAdd -> "Map.add"
  | PMRemove -> "Map.remove"
  | PMMem -> "Map.mem"
  | PMFind -> "Map.find"
  | PMIter -> "Map.iter"
  | PMMap -> "Map.map"
  | PMFold -> "Map.fold"
  | PMMapFold -> "Map.map_fold"
  | PBMEmpty _ -> "BigMap.empty"
  | PBMAdd -> "BigMap.add"
  | PBMRemove -> "BigMap.remove"
  | PBMMem -> "BigMap.mem"
  | PBMFind -> "BigMap.find"
  | PLLoop -> "Loop.loop"
  | PSLength -> "String.length"
  | PSConcat -> "String.concat"
  | PSSlice -> "String.slice"
  | PBLength -> "Bytes.length"
  | PBConcat -> "Bytes.concat"
  | PBSlice -> "Bytes.slice"
  | PBPack -> "Bytes.pack"
  | PBUnpack _ -> "Bytes.unpack"
  | PBHash -> "Bytes.hash"
  | PCBlake2b -> "Crypto.blake2b"
  | PCSha256 -> "Crypto.sha256"
  | PCSha512 -> "Crypto.sha512"
  | PCHashKey -> "Crypto.hash_key"
  | PCCheck -> "Crypto.check"
  | PCAddress -> "Contract.address"
  | PCSelf _ -> "Contract.self"
  | PCAt _ -> "Contract.at"
  | PCCall -> "Contract.call"
  | PCView -> "Contract.view"
  | PCCreate -> "Contract.create"
  | PCSetDeleg -> "Contract.set_delegate"
  | PADefault -> "Account.default"
  | PATransfer -> "Account.transfer"
  | PABalanceOf -> "Account.balance"
  | PAGetInfo -> "Account.get_info"
  | PAManage -> "Account.manage"
  | PCBalance -> "Current.balance"
  | PCTime -> "Current.time"
  | PCAmount -> "Current.amount"
  | PCGas -> "Current.gas"
  | PCrSelf -> "Current.self"
  | PCSource -> "Current.source"
  | PCSender -> "Current.sender"
  | PCLevel -> "Current.level"
  | PCCycle -> "Current.cycle"
  | PAddressOfKeyHash -> "Address.of_keyhash"
  | PKeyHashOfAddress -> "KeyHash.of_address"
  | PGenericPrimitive p -> p.prim_name

let string_of_kind = function
  | PrimFunction -> "function"
  | PrimPrefix -> "prefix"
  | PrimInfix -> "infix"

let same_kind k1 k2 = match k1, k2 with
  | PrimFunction, PrimFunction
  | PrimPrefix, PrimPrefix
  | PrimInfix, PrimInfix -> true
  | _ -> false

let number_of_static_primitives = 128
let max_possible_prim_id = 200
(* generic primitive by name. Used both for names and JSON encoding! *)
let generic_primitives_by_name = ref StringMap.empty
let prim_ids = Array.make (max_possible_prim_id + 1) None
let prim_arities = Array.make (max_possible_prim_id + 1) None
let max_prim_id = ref (number_of_static_primitives-1)

let from_string prim =
  StringMap.find_opt prim !generic_primitives_by_name

let check_generic_primitive kind prim =
  match StringMap.find_opt prim !generic_primitives_by_name with
  | Some p ->
      if not ( same_kind p.prim_kind kind ) then
        raise (GenericError (
            Format.asprintf "Primitive %S: %s, but found as %s"
              prim
              (string_of_kind p.prim_kind)
              (string_of_kind kind)));
      PGenericPrimitive p
  | None ->
      raise (GenericError (
          Format.asprintf "Unknown primitive %s" prim))

let of_string = function
  | "compare" -> PCompare
  | "iabs" -> PIAbs
  | "abs" -> PNAbs
  | "List.length" -> PLLength
  | "List.cons" -> PLCons
  | "List.rev" -> PLRev
  | "List.concat" -> PLConcat
  | "List.iter" -> PLIter
  | "List.map" -> PLMap
  | "List.fold" -> PLFold
  | "List.map_fold" -> PLMapFold
  | "Set.cardinal" -> PSCard
  | "Set.empty" -> PSEmpty
  | "Set.add" -> PSAdd
  | "Set.remove" -> PSRemove
  | "Set.mem" -> PSMem
  | "Set.iter" -> PSIter
  | "Set.map" -> PSMap
  | "Set.fold" -> PSFold
  | "Set.map_fold" -> PSMapFold
  | "Map.cardinal" -> PMCard
  | "Map.empty" -> PMEmpty
  | "Map.add" -> PMAdd
  | "Map.remove" -> PMRemove
  | "Map.mem" -> PMMem
  | "Map.find" -> PMFind
  | "Map.iter" -> PMIter
  | "Map.map" -> PMMap
  | "Map.fold" -> PMFold
  | "Map.map_fold" -> PMMapFold
  | "BigMap.empty" -> PBMEmpty (TUnit, TUnit)
  | "BigMap.add" -> PBMAdd
  | "BigMap.remove" -> PBMRemove
  | "BigMap.mem" -> PBMMem
  | "BigMap.find" -> PBMFind
  | "Loop.loop" -> PLLoop
  | "String.length" -> PSLength
  | "String.concat" -> PSConcat
  | "String.slice" -> PSSlice
  | "Bytes.length" -> PBLength
  | "Bytes.concat" -> PBConcat
  | "Bytes.slice" -> PBSlice
  | "Bytes.pack" -> PBPack
  | "Bytes.unpack" -> PBUnpack TUnit
  | "Bytes.hash" -> PBHash
  | "Crypto.blake2b" -> PCBlake2b
  | "Crypto.sha256" -> PCSha256
  | "Crypto.sha512" -> PCSha512
  | "Crypto.hash_key" -> PCHashKey
  | "Crypto.check" -> PCCheck
  | "Contract.address" -> PCAddress
  | "Contract.self" -> PCSelf Love_type.(StructType unit_contract_named_type)
  | "Contract.at" -> PCAt Love_type.(StructType unit_contract_named_type)
  | "Contract.call" -> PCCall
  | "Contract.view" -> PCView
  | "Contract.create" -> PCCreate
  | "Contract.set_delegate" -> PCSetDeleg
  | "Account.default" -> PADefault
  | "Account.transfer" -> PATransfer
  | "Account.balance" -> PABalanceOf
  | "Account.get_info" -> PAGetInfo
  | "Account.manage" -> PAManage
  | "Current.balance" -> PCBalance
  | "Current.time" -> PCTime
  | "Current.amount" -> PCAmount
  | "Current.gas" -> PCGas
  | "Current.self" -> PCrSelf
  | "Current.source" -> PCSource
  | "Current.sender" -> PCSender
  | "Current.level" -> PCLevel
  | "Current.cycle" -> PCCycle
  | "Address.of_keyhash" -> PAddressOfKeyHash
  | "KeyHash.of_address" -> PKeyHashOfAddress
  | prim -> check_generic_primitive PrimFunction prim

let infix_of_string = function
  | "=" -> PEq | "<>" -> PNe
  | "<" -> PLt | "<=" -> PLe
  | ">" -> PGt | ">=" -> PGe
  | "+" -> PIAdd | "-" -> PISub
  | "*" -> PIMul | "/" -> PIDiv
  | "++" -> PNAdd | "-+" -> PNSub
  | "*+" -> PNMul | "/+" -> PNDiv
  | "++!" -> PNIAdd | "+!+" -> PINAdd
  | "-+!" -> PNISub | "-!+" -> PINSub
  | "*+!" -> PNIMul | "*!+" -> PINMul
  | "/+!" -> PNIDiv | "/!+" -> PINDiv
  | "+$"  -> PDAdd  | "-$"  -> PDSub  | "/$"  -> PDDiv
  | "*$!" -> PDIMul | "*!$" -> PIDMul | "/$!" -> PDIDiv
  | "*$+" -> PDNMul | "*+$" -> PNDMul | "/$+" -> PDNDiv
  | "-:"  -> PTSub
  | "+:!" -> PTIAdd | "+!:" -> PITAdd | "-:!" -> PTISub
  | "+:+" -> PTNAdd | "++:" -> PNTAdd | "-:+" -> PTNSub
  | "&&"   -> PBAnd | "||"  -> PBOr | "|&" -> PBXor
  | "land" -> PIAnd | "lor" -> PIOr
  | "lxor" -> PIXor | "lnot" -> PINot
  | "lsl"  -> PILsl  | "lsr" -> PILsr
  | "nland" -> PNAnd | "nlor" -> PNOr | "nlxor" -> PNXor
  | "nlsl"  -> PNLsl | "nlsr" -> PNLsr
  | "^" -> PSConcat  | "@" -> PLConcat
  | prim -> check_generic_primitive PrimInfix prim

let prefix_of_string = function
  | "-" -> PINeg
  | "-+" -> PNNeg
  | "not" -> PBNot
  | "lnot" -> PINot
  | prim -> check_generic_primitive PrimPrefix prim

let is_infix = function
  | PEq | PNe | PLt | PLe | PGt | PGe
  | PIAdd | PISub | PIMul | PIDiv
  | PNAdd | PNSub | PNMul | PNDiv
  | PNIAdd | PINAdd | PNISub | PINSub
  | PNIMul | PINMul | PNIDiv | PINDiv
  | PDAdd | PDSub | PDDiv
  | PDIMul | PIDMul | PDIDiv
  | PDNMul | PNDMul | PDNDiv
  | PTSub
  | PTIAdd | PITAdd | PTISub
  | PTNAdd | PNTAdd | PTNSub
  | PBAnd | PBOr | PBXor
  | PIAnd | PIOr | PIXor | PILsl | PILsr
  | PNAnd | PNOr | PNXor | PNLsl | PNLsr
  | PSConcat -> true
  | PGenericPrimitive p -> same_kind p.prim_kind PrimInfix
  | _ -> false

let is_prefix = function
  | PINeg | PNNeg | PBNot | PINot -> true
  | PGenericPrimitive p -> same_kind p.prim_kind PrimPrefix
  | _ -> false

let type_of ?(sig_of_contract=(fun _ -> Love_type.unit_contract_sig)) =
  let open Love_type in
  let tv tv_name tv_traits = { tv_name; tv_traits } in
  let a = tv "_a" in
  let b = tv "_b" in
  let c = tv "_c" in
  let d = tv "_d" in
  function
  (* Comparisons *)
  | PCompare ->
    let a = a Love_type.comparable in let ta = TVar a in
    TForall (a, (ta @=> ta @=> TInt))

  | PEq  | PNe | PLt | PLe | PGt | PGe ->
    let a = a Love_type.comparable in let ta = TVar a in
    TForall (a, (ta @=> ta @=> TBool))

  (* Arithmetic on Integers *)
  | PIAdd | PISub | PIMul -> TInt @=> TInt @=> TInt
  | PIDiv                 -> TInt @=> TInt @=> TOption (TTuple [TInt; TNat])
  | PINeg                 -> TInt @=> TInt
  | PIAbs                 -> TInt @=> TInt

  (* Arithmetic on Naturals *)
  | PNAdd | PNMul         -> TNat @=> TNat @=> TNat
  | PNSub                 -> TNat @=> TNat @=> TInt
  | PNDiv                 -> TNat @=> TNat @=> TOption (TTuple [TNat; TNat])
  | PNNeg                 -> TNat @=> TInt
  | PNAbs                 -> TInt @=> TNat

  (* Arithmetic on Integers/Naturals *)
  | PNIAdd | PNISub | PNIMul -> TNat @=> TInt @=> TInt
  | PINAdd | PINSub | PINMul -> TInt @=> TNat @=> TInt
  | PNIDiv          -> TNat @=> TInt @=> TOption (TTuple [TInt; TNat])
  | PINDiv          -> TInt @=> TNat @=> TOption (TTuple [TInt; TNat])

  (* Arithmetic on Duns *)
  | PDAdd         -> TDun @=> TDun @=> TDun
  | PDSub         -> TDun @=> TDun @=> TOption TDun
  | PDDiv         -> TDun @=> TDun @=> TOption (TTuple [TInt; TDun])
  | PDIMul        -> TDun @=> TInt @=> TOption TDun
  | PIDMul        -> TInt @=> TDun @=> TOption TDun
  | PDIDiv        -> TDun @=> TInt @=> TOption (TTuple [TDun; TDun])
  | PDNMul        -> TDun @=> TNat @=> TDun
  | PNDMul        -> TNat @=> TDun @=> TDun
  | PDNDiv        -> TDun @=> TNat @=> TOption (TTuple [TDun; TDun])

  (* Arithmetic on Timestamps *)
  | PTSub           -> TTimestamp @=> TTimestamp @=> TInt
  | PTIAdd | PTISub -> TTimestamp @=> TInt @=> TTimestamp
  | PITAdd          -> TInt @=> TTimestamp @=> TTimestamp
  | PTNAdd | PTNSub -> TTimestamp @=> TNat @=> TTimestamp
  | PNTAdd          -> TNat @=> TTimestamp @=> TTimestamp

  (* Boolean operators *)
  | PBAnd | PBOr | PBXor -> TBool @=> TBool @=> TBool
  | PBNot                -> TBool @=> TBool

  (* Bitwise operators *)
  | PIAnd | PIOr | PIXor | PILsl | PILsr -> TInt @=> TInt @=> TInt
  | PNAnd | PNOr | PNXor | PNLsl | PNLsr -> TNat @=> TNat @=> TNat
  | PINot                                -> TInt @=> TInt

  (* Lists *)
  | PLLength ->
    let a = a Love_type.default_trait in let ta = TVar a in
    TForall (a, (TList ta @=> TNat))

  | PLRev ->
    let a = a Love_type.default_trait in let ta = TVar a in
    TForall (a, TList ta @=> TList ta)

  | PLConcat ->
    let a = a Love_type.default_trait in let ta = TVar a in
    TForall (a, TList ta @=> TList ta @=> TList ta)

  | PLCons ->
    let a = a Love_type.default_trait in let ta = TVar a in
    TForall (a, ta @=> TList ta @=> TList ta)

  | PLIter ->
    let a = a Love_type.default_trait in let ta = TVar a in
    TForall (a, (ta @=> TUnit) @=> TList ta @=> TUnit)

  | PLMap ->
    let a = a Love_type.default_trait in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b, (ta @=> tb) @=> TList ta @=> TList tb))

  | PLFold ->
    let a = a Love_type.default_trait in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b, (ta @=> tb @=> tb) @=> TList ta @=> tb @=> tb))

  | PLMapFold ->
    let a = a Love_type.default_trait in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    let c = c Love_type.default_trait in let tc = TVar c in
    TForall (a, TForall (b, TForall (c,
        (ta @=> tb @=> TTuple [tc; tb])
            @=> TList ta @=> tb @=> TTuple [TList tc; tb])))

  (* Sets *)
  | PSCard ->
    let a = a Love_type.comparable in let ta = TVar a in
    TForall (a, TSet ta @=> TNat)

  | PSEmpty ->
    let a = a Love_type.default_trait in let ta = TVar a in
    TForall (a, TSet ta)

  | PSAdd | PSRemove ->
    let a = a Love_type.comparable in let ta = TVar a in
    TForall (a, ta @=> TSet ta @=> TSet ta)

  | PSMem ->
    let a = a Love_type.comparable in let ta = TVar a in
    TForall (a, ta @=> TSet ta @=> TBool)

  | PSIter ->
    let a = a Love_type.comparable in let ta = TVar a in
    TForall (a, (ta @=> TUnit) @=> TSet ta @=> TUnit)

  | PSMap ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b, ((ta @=> tb)@=> TSet ta @=> TSet tb)))

  | PSFold ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b,
        (ta @=> tb @=> tb) @=> TSet ta @=> tb @=> tb))

  | PSMapFold ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    let c = c Love_type.default_trait in let tc = TVar c in
    TForall (a, TForall (b, TForall (c,
        (ta @=> tb @=> TTuple [tc; tb])
            @=> TSet ta @=> tb @=> TTuple [TSet tc; tb])))

  (* Maps *)
  | PMCard ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b, TMap (ta, tb) @=> TNat))

  | PMEmpty ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b, TMap (ta, tb)))

  | PMAdd ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b,
        ta @=> tb @=> TMap (ta, tb) @=> TMap (ta, tb)))

  | PMRemove ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, (TForall (b,
        ta @=> TMap (ta, tb) @=> TMap (ta, tb))))

  | PMMem ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b,
        ta @=> TMap (ta, tb) @=> TBool))

  | PMFind ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b,
        ta @=> TMap (ta, tb) @=> TOption tb))

  | PMIter ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b,
        (ta @=> tb @=> TUnit) @=> TMap (ta, tb) @=> TUnit))

  | PMMap ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    let c = c Love_type.default_trait in let tc = TVar c in
    TForall (a, TForall (b, TForall (c,
        (ta @=> tb @=> tc) @=> TMap (ta, tb) @=>  TMap (ta, tc))))

  | PMFold ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    let c = c Love_type.default_trait in let tc = TVar c in
    TForall (a, TForall (b, TForall (c,
        (ta @=> tb @=> tc @=> tc) @=> TMap (ta, tb) @=> tc @=> tc)))

  | PMMapFold ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    let c = c Love_type.default_trait in let tc = TVar c in
    let d = d Love_type.default_trait in let td = TVar d in
    TForall (a, TForall (b, TForall (c, TForall (d,
        (ta @=> tb @=> tc @=> TTuple [td; tc])
            @=> TMap (ta, tb) @=> tc @=> TTuple [TMap (ta,td); tc]))))


  (* Big maps *)
  | PBMEmpty (tk, td) -> TBigMap (tk, td)

  | PBMAdd ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, (TForall (b,
        ta @=> tb @=> TBigMap (ta, tb) @=> TBigMap (ta, tb))))

  | PBMRemove ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, (TForall (b,
        ta @=> TBigMap (ta, tb) @=> TBigMap (ta, tb))))

  | PBMMem ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, TForall (b,
        ta @=> TBigMap (ta, tb) @=> TBool))

  | PBMFind ->
    let a = a Love_type.comparable in let ta = TVar a in
    let b = b Love_type.default_trait in let tb = TVar b in
    TForall (a, (TForall (b,
        ta @=> TBigMap (ta, tb) @=> TOption tb)))

  (* Loop primitive *)
  | PLLoop ->
    let a = a Love_type.default_trait in let ta = TVar a in
    TForall (a, (ta @=> TTuple [TBool;ta]) @=> ta @=> ta)

  (* String primitives *)
  | PSLength -> TString @=> TNat
  | PSConcat -> TList TString @=> TString
  | PSSlice  -> TNat @=> TNat @=> TString @=> TOption TString

  (* Bytes primitives *)
  | PBLength    -> TBytes @=> TNat
  | PBConcat    -> TList TBytes @=> TBytes
  | PBSlice     -> TNat @=> TNat @=> TBytes @=> TOption TBytes
  | PBPack      ->
      let a = a Love_type.default_trait in
      TForall (a, TVar a @=> TBytes)
  | PBUnpack ty -> TBytes @=> TOption ty
  | PBHash      -> TBytes @=> TBytes

  (* Cryptographic operations *)
  | PCBlake2b -> TBytes @=> TBytes
  | PCSha256  -> TBytes @=> TBytes
  | PCSha512  -> TBytes @=> TBytes
  | PCHashKey -> TKey @=> TKeyHash
  | PCCheck   -> TKey @=> TSignature @=> TBytes @=> TBool

  (* Contract interactions *)
  | PCAddress       -> TContractInstance unit_contract_named_type @=> TAddress
  | PCSetDeleg      -> (TOption TKeyHash) @=> TOperation
  | PCSelf cnt_type ->
      let st = match cnt_type with
        | StructType st -> st
        | ContractInstance c -> Anonymous (sig_of_contract c)
      in
      TUnit @=> TOption (TContractInstance st)
  | PCAt cnt_type   ->
      let st = match cnt_type with
        | StructType st -> st
        | ContractInstance c -> Anonymous (sig_of_contract c)
      in
      TAddress @=> TOption (TContractInstance st)

  | PCCall ->
    let a = a Love_type.default_trait in let ptype = TVar a in
    TForall (a, TEntryPoint ptype @=> TDun @=> ptype @=> TOperation)

  | PCView ->
    let a = a Love_type.default_trait in let ptype = TVar a in
    let b = b Love_type.default_trait in let rtype = TVar b in
    TForall (a, TForall (b, TView (ptype, rtype) @=> ptype @=> rtype))

  | PCCreate ->
    let a = a Love_type.default_trait in let arg = TVar a in
    let ctype =
      TPackedStructure (
        Anonymous {unit_contract_sig with
                   sig_content = [
                     "storage", SType (SAbstract []);
                     "__init_storage",
                       SValue (arg @=> TUser (Ident.create_id "storage", []))
                   ]
                  }
      )
    in
    TForall (a, (TOption TKeyHash) @=> TDun @=> ctype @=>
                arg @=> TTuple [TOperation; TAddress])

  (* Account interactions *)
  | PADefault   ->
    TKeyHash @=>
    TContractInstance (
      Anonymous {
        unit_contract_sig with
        sig_content = [
          "default", SEntry TUnit
        ]
      }
    )
  | PATransfer  -> TAddress @=> TDun @=> TOperation
  | PABalanceOf -> TAddress @=> TDun
  | PAGetInfo ->
      TAddress @=> TTuple [TInt; TBool; TOption TAddress; TList TAddress]
  | PAManage ->
      TAddress @=> TOption TInt @=> TBool @=> TOption TAddress @=>
      TList TAddress @=> TOption TOperation

  (* General information *)
  | PCBalance | PCAmount           -> TUnit @=> TDun
  | PCrSelf | PCSource  | PCSender -> TUnit @=> TAddress
  | PCGas                          -> TUnit @=> TNat
  | PCCycle | PCLevel              -> TUnit @=> TNat
  | PCTime                         -> TUnit @=> TTimestamp

  (* Conversion *)
  | PAddressOfKeyHash    -> TKeyHash @=> TAddress
  | PKeyHashOfAddress    -> TAddress @=> TOption TKeyHash

  | PGenericPrimitive p -> p.prim_type

(** This function must not be changed : ids are used at serialization ! *)
let id_of_prim = function
  | PCompare            -> 0
  | PEq                 -> 1
  | PNe                 -> 2
  | PLt                 -> 3
  | PLe                 -> 4
  | PGt                 -> 5
  | PGe                 -> 6
  | PIAdd               -> 7
  | PISub               -> 8
  | PIMul               -> 9
  | PIDiv               -> 10
  | PINeg               -> 11
  | PIAbs               -> 12
  | PNAdd               -> 13
  | PNSub               -> 14
  | PNMul               -> 15
  | PNDiv               -> 16
  | PNNeg               -> 17
  | PNAbs               -> 18
  | PNIAdd              -> 19
  | PINAdd              -> 20
  | PNISub              -> 21
  | PINSub              -> 22
  | PNIMul              -> 23
  | PINMul              -> 24
  | PNIDiv              -> 25
  | PINDiv              -> 26
  | PDAdd               -> 27
  | PDSub               -> 28
  | PDDiv               -> 29
  | PDIMul              -> 30
  | PIDMul              -> 31
  | PDIDiv              -> 32
  | PDNMul              -> 33
  | PNDMul              -> 34
  | PDNDiv              -> 35
  | PTSub               -> 36
  | PTIAdd              -> 37
  | PITAdd              -> 38
  | PTISub              -> 39
  | PTNAdd              -> 40
  | PNTAdd              -> 41
  | PTNSub              -> 42
  | PBAnd               -> 43
  | PBOr                -> 44
  | PBXor               -> 45
  | PBNot               -> 46
  | PIAnd               -> 47
  | PIOr                -> 48
  | PIXor               -> 49
  | PINot               -> 50
  | PILsl               -> 51
  | PILsr               -> 52
  | PNAnd               -> 53
  | PNOr                -> 54
  | PNXor               -> 55
  | PNLsl               -> 56
  | PNLsr               -> 57
  | PLLength            -> 58
  | PLCons              -> 59
  | PLRev               -> 60
  | PLConcat            -> 61
  | PLIter              -> 62
  | PLMap               -> 63
  | PLFold              -> 64
  | PLMapFold           -> 65
  | PSEmpty             -> 66
  | PSCard              -> 67
  | PSAdd               -> 68
  | PSRemove            -> 69
  | PSMem               -> 70
  | PSIter              -> 71
  | PSMap               -> 72
  | PSFold              -> 73
  | PSMapFold           -> 74
  | PMEmpty             -> 75
  | PMCard              -> 76
  | PMAdd               -> 77
  | PMRemove            -> 78
  | PMMem               -> 79
  | PMFind              -> 80
  | PMIter              -> 81
  | PMMap               -> 82
  | PMFold              -> 83
  | PMMapFold           -> 84
  | PBMEmpty _          -> 85
  | PBMAdd              -> 86
  | PBMRemove           -> 87
  | PBMMem              -> 88
  | PBMFind             -> 89
  | PLLoop              -> 90
  | PSLength            -> 91
  | PSConcat            -> 92
  | PSSlice             -> 93
  | PBLength            -> 94
  | PBConcat            -> 95
  | PBSlice             -> 96
  | PBPack              -> 97
  | PBUnpack _          -> 98
  | PBHash              -> 99
  | PCBlake2b           -> 100
  | PCSha256            -> 101
  | PCSha512            -> 102
  | PCHashKey           -> 103
  | PCCheck             -> 104
  | PCAddress           -> 105
  | PCSelf _            -> 106
  | PCAt _              -> 107
  | PCCall              -> 108
  | PCView              -> 109
  | PCCreate            -> 110
  | PCSetDeleg          -> 111
  | PADefault           -> 112
  | PATransfer          -> 113
  | PABalanceOf         -> 114
  | PAGetInfo           -> 115
  | PAManage            -> 116
  | PCBalance           -> 117
  | PCTime              -> 118
  | PCAmount            -> 119
  | PCGas               -> 120
  | PCrSelf             -> 121
  | PCSource            -> 122
  | PCSender            -> 123
  | PCCycle             -> 124
  | PCLevel             -> 125
  | PAddressOfKeyHash   -> 126
  | PKeyHashOfAddress   -> 127

  | PGenericPrimitive p -> p.prim_id

let compare p1 p2 =
  Compare.Int.compare (id_of_prim p1) (id_of_prim p2)

let prim_list =
  [
    PCompare; PEq; PNe; PLt; PLe; PGt; PGe;
    PIAdd; PISub; PIMul; PIDiv; PINeg; PIAbs;
    PNAdd; PNSub; PNMul; PNDiv; PNNeg; PNAbs;
    PNIAdd; PINAdd; PNISub; PINSub;
    PNIMul; PINMul; PNIDiv; PINDiv;
    PDAdd; PDSub; PDDiv;
    PDIMul; PIDMul; PDIDiv;
    PDNMul; PNDMul; PDNDiv;
    PTSub;
    PTIAdd; PITAdd; PTISub;
    PTNAdd; PNTAdd; PTNSub;
    PBAnd; PBOr; PBXor; PBNot;
    PIAnd; PIOr; PIXor; PINot; PILsl; PILsr;
    PNAnd; PNOr; PNXor; PNLsl; PNLsr;
    PLLength; PLCons; PLRev; PLConcat; PLIter; PLMap; PLFold; PLMapFold;
    PSEmpty; PSCard; PSAdd; PSRemove; PSMem; PSIter; PSMap; PSFold; PSMapFold;
    PMEmpty; PMCard; PMAdd; PMRemove; PMMem; PMFind;
    PMIter; PMMap; PMFold; PMMapFold;
    PBMEmpty (TUnit, TUnit); PBMAdd; PBMRemove; PBMMem; PBMFind;
    PLLoop;
    PSLength; PSConcat; PSSlice;
    PBLength; PBConcat; PBSlice; PBPack; PBUnpack TUnit; PBHash;
    PCBlake2b; PCSha256; PCSha512; PCHashKey; PCCheck;
    PCAddress;
    PCSelf Love_type.(StructType unit_contract_named_type);
    PCAt Love_type.(StructType unit_contract_named_type);
    PCCall; PCView; PCCreate; PCSetDeleg;
    PADefault; PATransfer; PABalanceOf; PAGetInfo; PAManage;
    PCBalance; PCTime; PCAmount; PCGas;
    PCrSelf; PCSource; PCSender; PCCycle; PCLevel;
    PAddressOfKeyHash; PKeyHashOfAddress ]

let () =
  if Compare.Int.(number_of_static_primitives <> List.length prim_list) then
    raise (GenericError
             "Bad number of primitives: update in love_primitive.ml") ;

  List.iter (fun p ->
      let id = id_of_prim p in
      match prim_ids.( id ) with
      | Some _ ->
          raise (GenericError ("Duplicate primitive id " ^ string_of_int id))
      | None ->
          prim_ids.( id ) <- Some p ;
          let prim_arity =
            type_of p |> Love_type.remove_foralls |> Love_type.type_arity in
          prim_arities.( id ) <- Some prim_arity
    )
    prim_list

let arity = function
  | PGenericPrimitive p -> p.prim_arity
  | p ->
      match prim_arities.( id_of_prim p ) with
      | Some a -> a
      | None -> raise (GenericError ("Unknown primitive " ^ (name p)))

let prim_of_id id =
  if Compare.Int.( id > max_possible_prim_id ) then
    raise (GenericError ("Invalid primitive id " ^ (string_of_int id))) ;
  match prim_ids.( id ) with
  | Some a -> a
  | None ->
      raise (GenericError ("Unknown primitive id " ^ (string_of_int id)))

let add_primitive p =

  if Compare.Int.( p.prim_id < number_of_static_primitives ) then
    raise (GenericError (
        Format.asprintf "prim_id %d is below number_of_static_primitives %d"
          p.prim_id number_of_static_primitives ));

  if Compare.Int.( p.prim_id > max_possible_prim_id ) then
    raise (GenericError (
        Format.asprintf "prim_id %d is above max_possible_prim_id %d"
          p.prim_id max_possible_prim_id ));

  begin
    match prim_ids.(p.prim_id) with
    | None ->
        prim_ids.(p.prim_id) <- Some ( PGenericPrimitive p );
        prim_arities.(p.prim_id) <- Some p.prim_arity ;
    | Some _ ->
        raise (GenericError (
            Format.asprintf "prim_id %d used twice" p.prim_id ));
  end;

  begin
    match from_string p.prim_name with
    | None ->
        generic_primitives_by_name :=
          StringMap.add p.prim_name p !generic_primitives_by_name
    | Some _ ->
        raise (GenericError (
            Format.asprintf "prim_name %S used twice" p.prim_name ));
  end ;

  let computed_arity =
    p.prim_type |> Love_type.remove_foralls |> Love_type.type_arity
  in
  if Compare.Int.(p.prim_arity <> computed_arity) then
    raise (GenericError
             (Format.asprintf
                "Bad arity %d for primitive %S: computed %d"
                p.prim_arity p.prim_name computed_arity)) ;

  if Compare.Int.( p.prim_id > !max_prim_id ) then
    max_prim_id := p.prim_id
