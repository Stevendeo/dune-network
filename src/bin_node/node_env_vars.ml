(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Environment_variable

let env_info v =
  let doc = match v.allowed_values with
    | None -> v.description
    | Some allowed ->
        v.description ^ "\n$(b,Accepted values:) " ^
        String.concat ", " (List.map (Printf.sprintf "$(i,%s)") allowed) in
  Cmdliner.Term.env_info
    v.name
    ~doc

let envs = List.map env_info (Environment_variable.get_all ())
