(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_script_sig

type const = ..
type code = ..
type lang = Dune_script_sig.lang

module type S = sig
  module Internal : Dune_script_sig.S
  type const += Const of Internal.const
  type code += Code of Internal.code
  include Dune_script_sig.S
    with type const := Internal.const
     and type code := Internal.code
  val tag : int
  val const : Internal.const -> const
  val code : Internal.code -> code
  val is_const : const -> Internal.const option
  val is_code : code -> Internal.code option
  val lang_str : string
  val lang_encoding : lang Data_encoding.t

  (* to call from Dune_script_repr *)
  val init : unit -> unit
end

let languages = ref MapLang.empty

let lang_tag = ref 0

let lang_of_const = ref (fun _ -> raise Not_found)
let lang_of_code = ref (fun _ -> raise Not_found)

let close_registration () = lang_tag := -1

module Make0 (D : Dune_script_sig.S) = struct

  module Internal = D

  type const += Const of D.const
  type code += Code of D.code

  include Internal

  let tag = !lang_tag

  let const c = Const c
  let code c = Code c
  let is_const = function
    | Const c -> Some c
    | _ -> None
  let is_code = function
    | Code c -> Some c
    | _ -> None

  let lang_str =
    let { Dune_script_sig.name; version = v, x } = lang in
    Format.asprintf "%s.%d.%d" name v x

  let lang_encoding =
    let open Data_encoding in
    conv
      (fun { Dune_script_sig.name ; version = v, x } ->
         let { Dune_script_sig.name = name' ; version = w, y} = lang in
         assert
           (String.equal name name' && Compare.Int.(equal v w && equal x y));
         ())
      (fun () -> lang)
      (constant lang_str)

  let () =
    if Compare.Int.( !lang_tag < 0 ) then
      failwith "Module registers a language after close_registration()";
    incr lang_tag

  let init () = ()

end

module Make (D : Dune_script_sig.S) : S with module Internal := D = struct

  module M = Make0 (D)
  include M

  let init () =
    init ();
    languages := MapLang.add M.lang (module M : S) !languages

  let () =
    let old_lang_of_const = !lang_of_const in
    lang_of_const := function
      | Const _ -> (module M : S)
      | x -> old_lang_of_const x

  let () =
    let old_lang_of_code = !lang_of_code in
    lang_of_code := function
      | Code _ -> (module M : S)
      | x -> old_lang_of_code x

end

let lang_of_code code = !lang_of_code code
let lang_of_const const = !lang_of_const const
