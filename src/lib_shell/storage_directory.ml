(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Storage_services

let state_block_read_exn chain_state ~pred head =
  if pred = 0 then
    Lwt.return_some head
  else
    State.Block.read_predecessor
      chain_state ~pred ~below_save_point:true (State.Block.hash head)

let storage_gc state q () =
  let keep_blocks = match q.keep with
    | None -> 9 * 4096
    | Some n ->
        (* we always keep at least 100 blocks !! *)
        if n < 100 then 100 else n
  in
  Printf.eprintf "Garbage collection requested by RPC\n%!";
  let chain_id = State.Chain.main state in
  State.Chain.get_exn state chain_id >>= fun chain_state ->
  Chain.head chain_state >>= fun head ->
  state_block_read_exn chain_state ~pred:keep_blocks head >|=
  Option.unopt_assert ~loc:__POS__ >>= State.Block.gc >>= fun () ->
  return ()

let storage_revert state () () =
  Printf.eprintf "Revert to Irmin requested by RPC\n%!";
  let chain_id = State.Chain.main state in
  State.Chain.get_exn state chain_id >>= fun chain_state ->
  State.revert chain_state >>= fun () ->
  return ()

let build_rpc_directory state =
  let dir = ref RPC_directory.empty in
  let register0 s f =
    dir := RPC_directory.register !dir s (fun () p q -> f p q)
  in
  register0 Storage_services.storage_gc (storage_gc state);
  register0 Storage_services.storage_revert (storage_revert state);
  !dir
