

Sep 16, 2019: Merge with Tezos (e725003d107395c2d78c6633e9d3a4f5f127f6bf)
* P2p :
  * Tag nodes as public/private even if we reject the connection.
  * Don't advertize banned points
  * Random and parallel kill strategy on too_many_connection
  * Add randomness in replies to Bootstrap messages
  * Max greylisting to 2 days
  * Always run a maintenance when activating the worker
  * "greylisting_config" in p2p config
   * reload known points info from peers.json on startup
* Ledger:
   * Allow non-hardened paths (only for dn2 and dn3)
   * Allow 'h' as an alternative syntax to '\''
   * Add support for the Legder Nano X
   * Check app version for each derivation kind
   * Support for Bip32_ed25519
   * Add support for deterministic nonces
* Baker:
   * include all available operations once enough endorsements have arrived
   * Add a version to POW when creating a block "\x00\x00\x00\x03"
* Shell: monitor_mempool has default values for  parameters now
* Michelson:
   * rename field labels in Michelson typechecker output
    "stackBefore" -> "stack_before", "stackAfter" -> "stack_after"

Sep 10, 2019: Initial Release
* New operations for airdrop and protocol control
* Support for Dune Ledger app
* Scripts can be referred to by hash in originations
* Batch transfers in dune-client and some other new commands
* Rolls at 10,000 DUN on Mainnet
* New Michelson instructions
* New RPCs
* Fee code in smart contracts
* Signer: new proxy mode
* GPLv3 License

Jun 6, 2019: Fork of Tezos (commit 20ce5a625781c6abbaaefdb9e7c8896aba799a7a)

