#!/bin/bash

# set -e

run_scripts_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

if ! which jq > /dev/null 2>&1 ; then
    echo "+-+-+-"
    echo "This script needs the jq tool. Install it as follows:"
    echo "sudo apt install jq"
    echo "+-+-+-"
    exit 1
fi

if [ -z $GENESIS_BAKER ]; then
    >&2 echo "Warning: undefined GENESIS_BAKER"
fi
if [ -z $UNSAFE_BAKER ]; then
    >&2 echo "Warning: undefined UNSAFE_BAKER"
fi
if [ -z $SAFE_LOCAL_LEDGER ]; then
    >&2 echo "Warning: undefined SAFE_LOCAL_LEDGER"
fi
if [ -z $SAFE_REMOTE_SIGNER ]; then
    >&2 echo "Warning: undefined SAFE_REMOTE_SIGNER"
fi

tmpdir=`mktemp -d`
touch $tmpdir/blocks
while true
do
      $run_scripts_dir/01-dune-node.sh head > $tmpdir/log 2> /dev/null
      tail -n +2 $tmpdir/log > $tmpdir/head.headers
      level=`jq .header.level $tmpdir/head.headers 2> /dev/null`
      priority=`jq .header.priority $tmpdir/head.headers 2> /dev/null`
      baker=`jq -r .metadata.baker $tmpdir/head.headers 2> /dev/null`
      cycle=`jq .metadata.level.cycle $tmpdir/head.headers 2> /dev/null`
      entry="lvl=$level, prio=$priority, cycle=$cycle, baker=$baker"
      #echo "this ? $entry"
      if [ "`grep -c \"$entry\" $tmpdir/blocks`" -eq "0" ]
      then
          echo $entry >> $tmpdir/blocks

          case $baker in
              "")
                  echo $entry
                  ;;
              "$GENESIS_BAKER")
                  # Cyan
                  echo -e "\e[36m$entry\e[0m"
                  ;;
              "$UNSAFE_BAKER")
                  # Red
                  echo -e "\e[31m$entry\e[0m"
                  ;;
              "$SAFE_LOCAL_LEDGER")
                  # "Green"
                  echo -e "\e[32m$entry\e[0m"
                  ;;
              "$SAFE_REMOTE_SIGNER")
                  # Blue
                  echo -e "\e[34m$entry\e[0m"
                  ;;
              *)
                  echo $entry
                  ;;
          esac
      fi
      sleep 1
done
